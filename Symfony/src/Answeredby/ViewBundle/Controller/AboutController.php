<?php

namespace Answeredby\ViewBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;

class AboutController extends SharedBundleController
{
    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $whoWeAre = array();
        $whoWeAre['Founder & CEO'] = $entityManager->getRepository('AnsweredbyEntityBundle:User')->findOneByEmail('pat.zabawa@answeredby.com');

        return $this->render('AnsweredbyViewBundle:About:index.html.twig', array('whoWeAre' => $whoWeAre, 'doNotShowSidebar' => TRUE));
    }
}
