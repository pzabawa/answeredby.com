<?php

namespace Answeredby\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TermsController extends Controller
{
    public function indexAction()
    {
        return $this->render('AnsweredbyViewBundle:Terms:index.html.twig', array('doNotShowSidebar' => TRUE));
    }
}