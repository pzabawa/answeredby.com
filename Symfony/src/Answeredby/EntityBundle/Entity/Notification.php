<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="notifications")
 */
class Notification extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="md5", type="string", length=255, unique=true)
     */
    protected $md5;

    /**
     * @ORM\ManyToOne(targetEntity="UserPost", inversedBy="notifications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="new_userpost_user_id", referencedColumnName="created_user_id", nullable=true),
     *   @ORM\JoinColumn(name="new_userpost_post_id", referencedColumnName="post_id", nullable=true)
     * })
     */
    protected $new_userpost;

    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="notifications")
     * @ORM\JoinColumn(name="new_subpost_id", referencedColumnName="id", nullable=true)
     */
    protected $new_subpost;

    /**
     * @ORM\ManyToOne(targetEntity="PostShare", inversedBy="notifications")
     * @ORM\JoinColumn(name="new_postshare_id", referencedColumnName="id", nullable=true)
     */
    protected $new_postshare;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="notifications")
     * @ORM\JoinColumn(name="destination_user_id", referencedColumnName="id", nullable=false)
     */
    protected $destination_user;

    /**
     * @ORM\Column(name="viewed", type="boolean", nullable=false, options={"default" = 0})
     */
    protected $viewed;

    /**
     * @ORM\Column(name="opened", type="boolean", nullable=false, options={"default" = 0})
     */
    protected $opened;

    /**
     * @ORM\Column(name="wasEmailSent", type="boolean")
     */
    protected $wasEmailSent;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdNotifications")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser,
                                \Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        $this->_setMd5($this->getRandomToken());
        $this->_setDestinationUser($destinationUser);
        $this->setViewed(FALSE);
        $this->setOpened(FALSE);
        $this->setWasEmailSent(FALSE);
        
        parent::__construct($createdUser);
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Notification
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set md5
     *
     * @param string $md5
     * @return Notification
     */
    protected function _setMd5($md5)
    {
        $this->md5 = $md5;

        return $this;
    }

    /**
     * Get md5
     *
     * @return string 
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * Set viewed
     *
     * @param boolean $viewed
     * @return Notification
     */
    public function setViewed($viewed)
    {
        $this->viewed = $viewed;
    
        return $this;
    }

    /**
     * Get viewed
     *
     * @return boolean 
     */
    public function getViewed()
    {
        return $this->viewed;
    }

    /**
     * Set opened
     *
     * @param boolean $opened
     * @return Notification
     */
    public function setOpened($opened)
    {
        $this->opened = $opened;
    
        return $this;
    }

    /**
     * Get opened
     *
     * @return boolean 
     */
    public function getOpened()
    {
        return $this->opened;
    }

    /**
     * Set new_subpost
     *
     * @param \Answeredby\EntityBundle\Entity\Post $newSubpost
     * @return Notification
     */
    public function setNewSubpost(\Answeredby\EntityBundle\Entity\Post $newSubpost = null)
    {
        $this->new_subpost = $newSubpost;

        return $this;
    }

    /**
     * Get new_subpost
     *
     * @return \Answeredby\EntityBundle\Entity\Post 
     */
    public function getNewSubpost()
    {
        return $this->new_subpost;
    }

    /**
     * Set new_postshare
     *
     * @param \Answeredby\EntityBundle\Entity\PostShare $newPostshare
     * @return Notification
     */
    public function setNewPostshare(\Answeredby\EntityBundle\Entity\PostShare $newPostshare = null)
    {
        $this->new_postshare = $newPostshare;

        return $this;
    }

    /**
     * Get new_postshare
     *
     * @return \Answeredby\EntityBundle\Entity\PostShare 
     */
    public function getNewPostshare()
    {
        return $this->new_postshare;
    }

    /**
     * Set destination_user
     *
     * @param \Answeredby\EntityBundle\Entity\User $destinationUser
     * @return Notification
     */
    protected function _setDestinationUser(\Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        $this->destination_user = $destinationUser;

        return $this;
    }

    /**
     * Get destination_user
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getDestinationUser()
    {
        return $this->destination_user;
    }

    /**
     * Set new_userpost
     *
     * @param \Answeredby\EntityBundle\Entity\UserPost $newUserpost
     * @return Notification
     */
    public function setNewUserpost(\Answeredby\EntityBundle\Entity\UserPost $newUserpost = null)
    {
        $this->new_userpost = $newUserpost;

        return $this;
    }

    /**
     * Get new_userpost
     *
     * @return \Answeredby\EntityBundle\Entity\UserPost 
     */
    public function getNewUserpost()
    {
        return $this->new_userpost;
    }

    /**
     * Set wasEmailSent
     *
     * @param boolean $wasEmailSent
     * @return Notification
     */
    public function setWasEmailSent($wasEmailSent)
    {
        $this->wasEmailSent = $wasEmailSent;

        return $this;
    }

    /**
     * Get wasEmailSent
     *
     * @return boolean
     */
    public function getWasEmailSent()
    {
        return $this->wasEmailSent;
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Notification
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Get a description of the Notification
     *
     * @return \Answeredby\EntityBundle\Entity\Post or NULL
     */
    public function getTargetPost()
    {
       if( $this->getNewSubpost() )
           return $this->getNewSubpost()->getParentPost();
       if( $this->getNewPostshare() )
           return $this->getNewPostshare()->getPost();
       if( $this->getNewUserpost() )
           return $this->getNewUserpost()->getPost();
       return NULL;
    }

    /**
     * Get a description of the Notification
     *
     * @return string
     */
    public function getDescription()
    {
       if( $this->getNewSubpost() )
           return $this->getCreatedUser()->getUsername() . " commented on your post.";
       if( $this->getNewPostshare() )
           return $this->getCreatedUser()->getUsername() . " shared a post with you.";
       if( $this->getNewUserpost() )
           return $this->getCreatedUser()->getUsername() . " voted up your post.";
       return $this->getCreatedUser()->getUsername() . " invited you to <b><span class='blue'>Answered</span><span class='orange'>By</span></b> via e-mail.";
    }
}
