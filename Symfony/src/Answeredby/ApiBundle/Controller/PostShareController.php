<?php

namespace Answeredby\ApiBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\PostShare;

class PostShareController extends SharedBundleController
{
    protected function _postSharesToPostShareStdClasses($postShares)
    {
        $postShareStdClasses = array();
        foreach( $postShares AS $postShare ){
            $postShareStdClass = new \stdClass();
            $postShareStdClass->message = $postShare->getMessage() ? $postShare->getMessage() : "";
            $postShareStdClass->createdDatetime = $postShare->getCreatedDatetime()->format('Y-m-d\\TH:i:sP');
            $postShareStdClass->networks = array();
            foreach( $postShare->getNetworks() AS $postShareNetwork ){
                $postShareNetworkStdClass = new \stdClass();
                $postShareNetworkStdClass->text = $postShareNetwork->getText();
                $postShareNetworkStdClass->destinationUsernames = "";
                if( $postShareNetwork->getIsIndividualSharesType() ){
                    foreach( $postShare->getPostShareDestinationUsers() AS $postShareDestinationUser )
                        $postShareNetworkStdClass->destinationUsernames .= "\n" . $postShareDestinationUser->getDestinationUser()->getUsername();
                } else {
                    foreach( $postShareNetwork->getRelationships() AS $relationship ){
                        if( $relationship->getApprovedByOriginationUser()
                         && $relationship->getApprovedByDestinationUser() )
                            $postShareNetworkStdClass->destinationUsernames .= "\n" . $relationship->getDestinationUser()->getUsername();
                    }
                }
                $postShareNetworkStdClass->destinationUsernames = $postShareNetworkStdClass->destinationUsernames
                                                                  ? substr($postShareNetworkStdClass->destinationUsernames, 1)
                                                                  : "(No approved members)";
                $postShareStdClass->networks[] = $postShareNetworkStdClass;
            }
            $postShareStdClasses[] = $postShareStdClass;
        }
        return $postShareStdClasses;
    }

    public function postAction()
    {
        if( !empty($_POST) ){
            $entityManager = $this->getDoctrine()->getManager();

            $postShare = new PostShare($this->getUser());
            $postShare->setPost($entityManager->getReference('AnsweredbyEntityBundle:Post', filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT)));
            $postShare->setMessage(filter_input(INPUT_POST, 'message', FILTER_SANITIZE_STRING));

            $entityManager->persist($postShare);
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            try {
                $this->_processPostSharePost($postShare);
            } catch (\Exception $e) {
                return new Response(json_encode(array('message' => $e->getMessage())), 200, array('Content-Type'=>'application/json'));
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            return new Response(json_encode(array('postShares' => $this->_postSharesToPostShareStdClasses(array($postShare)))), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }

    public function getAction($post_id)
    {
        $postShares = $this->getDoctrine()
                ->getRepository('AnsweredbyEntityBundle:PostShare')
                ->createQueryBuilder('postShare')
                ->where('postShare.post = :postId')
                ->setParameter('postId', $post_id)
                ->andWhere('postShare.createdUser = :currentUser')
                ->setParameter('currentUser', $this->getUser())
                ->orderBy('postShare.createdDatetime', 'DESC')
                ->getQuery()
                ->getResult();
        return new Response(json_encode(array('postShares' => $this->_postSharesToPostShareStdClasses($postShares))), 200, array('Content-Type'=>'application/json'));
    }
}
