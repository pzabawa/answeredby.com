<?php

namespace Answeredby\ViewBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;

class RegisterController extends SharedBundleController
{
    public function indexAction()
    {
        return $this->_loginAction('AnsweredbyViewBundle:Register:index.html.twig');
    }
}
