<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="userSuggestionHides")
 */
class UserSuggestionHide extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="destination_user_id", referencedColumnName="id", nullable=false)
     */
    protected $destination_user;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser,
                                \Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        $this->_setDestinationUser($destinationUser);
        parent::__construct($createdUser);
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return UserSuggestionHide
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set destination_user
     *
     * @param \Answeredby\EntityBundle\Entity\User $destinationUser
     * @return UserSuggestionHide
     */
    protected function _setDestinationUser(\Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        $this->destination_user = $destinationUser;

        return $this;
    }

    /**
     * Get destination_user
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getDestinationUser()
    {
        return $this->destination_user;
    }
}
