<?php

namespace Answeredby\ViewBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;

class UserController extends SharedBundleController
{
    public function indexAction($userid, $username)
    {
        $userObject = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->find($userid);

        if( !is_a($userObject, 'Answeredby\EntityBundle\Entity\User') ){
            throw $this->createNotFoundException('No user found.');
        }

        // Correct the URL if the id matches but the text doesn't.
        if( $username !== $userObject->getUrlUsername() )
            return $this->redirect($this->generateUrl('UserPage', array('userid' => $userObject->getId(), 'username' => $userObject->getUrlUsername())), 301);

        if( $this->_canCurrentUserAccessUserPageOfUser($userObject) ){
            if( $this->getUser() )
                return $this->render('AnsweredbyViewBundle:User:index.html.twig', array('user' => $userObject));
            return $this->render('AnsweredbyViewBundle:User:index.html.twig', array('user' => $userObject));
        }

        return $this->render('AnsweredbyViewBundle:User:index_noaccess.html.twig', array('user' => $userObject));
    }
}
