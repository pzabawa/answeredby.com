<?php

namespace Answeredby\SecurityBundle\Handler;

use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerAware;

class LogoutSuccessHandler extends ContainerAware implements LogoutSuccessHandlerInterface
{
    public function onLogoutSuccess(Request $request)
    {
        // Remove the session targets used only for the login page.
        $session = $request->getSession();
        $session->remove('target_url');
        $session->remove('target_fragment');

        // Redirect the user to the page they were on.
        $target_url = $request->query->get('target_url')
                      ? $request->query->get('target_url')
                      : "/";
        return new RedirectResponse($target_url);
    }
}