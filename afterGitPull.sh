#!/bin/bash

# reset database
/vagrant/Symfony/app/console doctrine:schema:update --force

# clear cache
rm -rf /var/Symfony/cache/*
rm -rf /var/Symfony/logs/*
rm -rf /var/Symfony/sessions/*

# reset crons
crontab -u www-data /vagrant/cron

