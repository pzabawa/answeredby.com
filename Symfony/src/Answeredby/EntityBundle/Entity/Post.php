<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="posts")
 */
class Post extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="text", type="text")
     */
    protected $text;

    /**
     * @ORM\Column(name="hidden_text", type="text", nullable=true)
     */
    protected $hiddenText;

    /**
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * @ORM\Column(name="attachment_title", type="string", length=255, nullable=true)
     */
    protected $attachment_title;

    /**
     * @ORM\Column(name="attachment_description", type="string", length=255, nullable=true)
     */
    protected $attachment_description;

    /**
     * @ORM\Column(name="attachment_is_shown", type="boolean")
     */
    protected $attachment_is_shown;

    /**
     * @ORM\Column(name="attachment_url", type="string", length=255)
     */
    protected $attachment_url;

    /**
     * @ORM\Column(name="attachment_image_url", type="string", length=255)
     */
    protected $attachment_image_url;

    /**
     * @ORM\Column(name="attachment_video_url", type="string", length=255)
     */
    protected $attachment_video_url;

    /**
     * @ORM\Column(name="attachment_video_height", type="string", length=255)
     */
    protected $attachment_video_height;

    /**
     * @ORM\Column(name="attachment_video_width", type="string", length=255)
     */
    protected $attachment_video_width;

    /**
     * @ORM\Column(name="is_public", type="boolean")
     */
    protected $is_public;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;


    /**
     * @ORM\OneToMany(targetEntity="PostShare", mappedBy="post")
     **/
    protected $postShares;

    /**
     * @ORM\OneToMany(targetEntity="UserPost", mappedBy="post", cascade={"remove"})
     **/
    protected $userPosts;

    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="childrenPosts")
     * @ORM\JoinColumn(name="parent_post_id", referencedColumnName="id")
     */
    protected $parentPost;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="parentPost", cascade={"remove"})
     * @ORM\OrderBy({"createdDatetime" = "ASC"})
     **/
    protected $childrenPosts;

    /**
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="new_subpost", cascade={"persist"})
     **/
    protected $notifications;

    /**
     * @ORM\OneToMany(targetEntity="UserPostHide", mappedBy="post")
     **/
    protected $userPostHides;

    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="post", cascade={"persist"})
     **/
    protected $photos;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->postShares = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userPosts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->childrenPosts = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notifications = new \Doctrine\Common\Collections\ArrayCollection();
        $this->userPostHides = new \Doctrine\Common\Collections\ArrayCollection();
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($createdUser);
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Post
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Post
     */
    public function setText($text)
    {
        $this->text = rtrim($text);

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set hiddenText
     *
     * @param string $hiddenText
     * @return Post
     */
    public function setHiddenText($hiddenText)
    {
        $this->hiddenText = $hiddenText;

        return $this;
    }

    /**
     * Get hiddenText
     *
     * @return string 
     */
    public function getHiddenText()
    {
        return $this->hiddenText;
    }

    /**
     * Set is_public
     *
     * @param boolean $isPublic
     * @return Post
     */
    public function setIsPublic($isPublic)
    {
        $this->is_public = $isPublic;

        return $this;
    }

    /**
     * Get is_public
     *
     * @return boolean 
     */
    public function getIsPublic()
    {
        return $this->is_public;
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Post
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add postShares
     *
     * @param \Answeredby\EntityBundle\Entity\PostShare $postShares
     * @return Post
     */
    public function addPostShare(\Answeredby\EntityBundle\Entity\PostShare $postShares)
    {
        $this->postShares[] = $postShares;

        return $this;
    }

    /**
     * Remove postShares
     *
     * @param \Answeredby\EntityBundle\Entity\PostShare $postShares
     */
    public function removePostShare(\Answeredby\EntityBundle\Entity\PostShare $postShares)
    {
        $this->postShares->removeElement($postShares);
    }

    /**
     * Get postShares
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPostShares()
    {
        return $this->postShares;
    }

    /**
     * Get active postShares created by User $user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPostSharesCreatedBy($user)
    {
        return $this->getPostShares()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("createdUser", $user)));
    }

    /**
     * Add userPosts
     *
     * @param \Answeredby\EntityBundle\Entity\UserPost $userPosts
     * @return Post
     */
    public function addUserPost(\Answeredby\EntityBundle\Entity\UserPost $userPosts)
    {
        $this->userPosts[] = $userPosts;

        return $this;
    }

    /**
     * Remove userPosts
     *
     * @param \Answeredby\EntityBundle\Entity\UserPost $userPosts
     */
    public function removeUserPost(\Answeredby\EntityBundle\Entity\UserPost $userPosts)
    {
        $this->userPosts->removeElement($userPosts);
    }

    /**
     * Get userPosts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserPosts()
    {
        return $this->userPosts;
    }

    /**
     * Set parentPost
     *
     * @param \Answeredby\EntityBundle\Entity\Post $parentPost
     * @return Post
     */
    public function setParentPost(\Answeredby\EntityBundle\Entity\Post $parentPost)
    {
        $this->parentPost = $parentPost;

        return $this;
    }

    /**
     * Get parentPost
     *
     * @return \Answeredby\EntityBundle\Entity\Post 
     */
    public function getParentPost()
    {
        return $this->parentPost;
    }

    /**
     * Add childrenPosts
     *
     * @param \Answeredby\EntityBundle\Entity\Post $childrenPosts
     * @return Post
     */
    public function addChildrenPost(\Answeredby\EntityBundle\Entity\Post $childrenPosts)
    {
        $this->childrenPosts[] = $childrenPosts;

        return $this;
    }

    /**
     * Remove childrenPosts
     *
     * @param \Answeredby\EntityBundle\Entity\Post $childrenPosts
     */
    public function removeChildrenPost(\Answeredby\EntityBundle\Entity\Post $childrenPosts)
    {
        $this->childrenPosts->removeElement($childrenPosts);
    }

    /**
     * Get childrenPosts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getChildrenPosts()
    {
        return $this->childrenPosts;
    }

    /**
     * Get userPosts created by User $user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserPostsCreatedBy($user)
    {
        return $this->getUserPosts()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("createdUser", $user)));
    }

    /**
     * Set attachment_title
     *
     * @param string $attachmentTitle
     * @return Post
     */
    public function setAttachmentTitle($attachmentTitle)
    {
        $this->attachment_title = $attachmentTitle;

        return $this;
    }

    /**
     * Get attachment_title
     *
     * @return string 
     */
    public function getAttachmentTitle()
    {
        return $this->attachment_title;
    }

    /**
     * Set attachment_description
     *
     * @param string $attachmentDescription
     * @return Post
     */
    public function setAttachmentDescription($attachmentDescription)
    {
        $this->attachment_description = $attachmentDescription;

        return $this;
    }

    /**
     * Get attachment_description
     *
     * @return string 
     */
    public function getAttachmentDescription()
    {
        return $this->attachment_description;
    }

    /**
     * Set attachment_url
     *
     * @param string $attachmentUrl
     * @return Post
     */
    public function setAttachmentUrl($attachmentUrl)
    {
        $this->attachment_url = $attachmentUrl;

        return $this;
    }

    /**
     * Get attachment_url
     *
     * @return string 
     */
    public function getAttachmentUrl()
    {
        return $this->attachment_url;
    }

    /**
     * Set attachment_image_url
     *
     * @param string $attachmentImageUrl
     * @return Post
     */
    public function setAttachmentImageUrl($attachmentImageUrl)
    {
        $this->attachment_image_url = $attachmentImageUrl;

        return $this;
    }

    /**
     * Get attachment_image_url
     *
     * @return string 
     */
    public function getAttachmentImageUrl()
    {
        return $this->attachment_image_url;
    }

    /**
     * Set attachment_video_url
     *
     * @param string $attachmentVideoUrl
     * @return Post
     */
    public function setAttachmentVideoUrl($attachmentVideoUrl)
    {
        $this->attachment_video_url = $attachmentVideoUrl;

        return $this;
    }

    /**
     * Get attachment_video_url
     *
     * @return string 
     */
    public function getAttachmentVideoUrl()
    {
        return $this->attachment_video_url;
    }

    /**
     * Set attachment_video_height
     *
     * @param string $attachmentVideoHeight
     * @return Post
     */
    public function setAttachmentVideoHeight($attachmentVideoHeight)
    {
        $this->attachment_video_height = $attachmentVideoHeight;

        return $this;
    }

    /**
     * Get attachment_video_height
     *
     * @return string 
     */
    public function getAttachmentVideoHeight()
    {
        return $this->attachment_video_height;
    }

    /**
     * Set attachment_video_width
     *
     * @param string $attachmentVideoWidth
     * @return Post
     */
    public function setAttachmentVideoWidth($attachmentVideoWidth)
    {
        $this->attachment_video_width = $attachmentVideoWidth;

        return $this;
    }

    /**
     * Get attachment_video_width
     *
     * @return string 
     */
    public function getAttachmentVideoWidth()
    {
        return $this->attachment_video_width;
    }

    /**
     * Set attachment_is_shown
     *
     * @param boolean $attachmentIsShown
     * @return Post
     */
    public function setAttachmentIsShown($attachmentIsShown)
    {
        $this->attachment_is_shown = $attachmentIsShown;

        return $this;
    }

    /**
     * Get attachment_is_shown
     *
     * @return boolean 
     */
    public function getAttachmentIsShown()
    {
        return $this->attachment_is_shown;
    }

    public function constructNotifications()
    {
        if( $this->getParentPost() && $this->getCreatedUser() != $this->getParentPost()->getCreatedUser() ){
            $notification = new Notification($this->getCreatedUser(),
                                             $this->getParentPost()->getCreatedUser());
            $notification->setNewSubpost($this);
            $this->addNotification($notification);
        }
    }

    /**
     * Add notifications
     *
     * @param \Answeredby\EntityBundle\Entity\Notification $notifications
     * @return Post
     */
    public function addNotification(\Answeredby\EntityBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \Answeredby\EntityBundle\Entity\Notification $notifications
     */
    public function removeNotification(\Answeredby\EntityBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Add userPostHides
     *
     * @param \Answeredby\EntityBundle\Entity\UserPostHide $userPostHides
     * @return Post
     */
    public function addUserPostHide(\Answeredby\EntityBundle\Entity\UserPostHide $userPostHides)
    {
        $this->userPostHides[] = $userPostHides;

        return $this;
    }

    /**
     * Remove userPostHides
     *
     * @param \Answeredby\EntityBundle\Entity\UserPostHide $userPostHides
     */
    public function removeUserPostHide(\Answeredby\EntityBundle\Entity\UserPostHide $userPostHides)
    {
        $this->userPostHides->removeElement($userPostHides);
    }

    /**
     * Get userPostHides
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserPostHides()
    {
        return $this->userPostHides;
    }

    /**
     * Add photos
     *
     * @param \Answeredby\EntityBundle\Entity\Photo $photo
     * @return Post
     */
    public function addPhoto(\Answeredby\EntityBundle\Entity\Photo $photo)
    {
        $this->photos[] = $photo;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \Answeredby\EntityBundle\Entity\Photo $photo
     */
    public function removePhoto(\Answeredby\EntityBundle\Entity\Photo $photo)
    {
        $this->photos->removeElement($photo);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }
}
