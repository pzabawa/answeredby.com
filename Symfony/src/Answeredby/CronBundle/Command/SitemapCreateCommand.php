<?php

// To run:
// $ app/console sitemap:create

namespace Answeredby\CronBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mailgun\Mailgun;

class SitemapCreateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('sitemap:create')->setDescription('Create answer1answer2news sitemap.xml');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sitemapxml = 
            '<?xml version="1.0" encoding="UTF-8"?>' .
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' .
            '<url>' .
            '<loc>http://www.answeredby.com</loc>' .
            '<changefreq>daily</changefreq>' .
            '<priority>1.0</priority>' .
            '</url>' .
            '<url>' .
            '<loc>http://www.answeredby.com/about</loc>' .
            '<lastmod>2014-04-15</lastmod>' .
            '<changefreq>monthly</changefreq>' .
            '</url>' .
            '<url>' .
            '<loc>http://www.answeredby.com/contact</loc>' .
            '<lastmod>2014-04-09</lastmod>' .
            '<changefreq>monthly</changefreq>' .
            '</url>' .
            '<url>' .
            '<loc>http://www.answeredby.com/login</loc>' .
            '<lastmod>2014-04-05</lastmod>' .
            '<changefreq>monthly</changefreq>' .
            '<priority>0.1</priority>' .
            '</url>' .
            '<url>' .
            '<loc>http://www.answeredby.com/terms</loc>' .
            '<lastmod>2014-04-09</lastmod>' .
            '<changefreq>monthly</changefreq>' .
            '<priority>0.1</priority>' .
            '</url>' .
            '<url>' .
            '<loc>http://www.answeredby.com/privacy</loc>' .
            '<lastmod>2014-04-09</lastmod>' .
            '<changefreq>monthly</changefreq>' .
            '<priority>0.1</priority>' .
            '</url>' .
            '</urlset>';

        $fileLocation = $this->getContainer()->get('kernel')->getRootDir() . "/../web/sitemap.xml";
        if( file_exists($fileLocation) )
            unlink($fileLocation);
        $fileSaveResult = file_put_contents($fileLocation, $sitemapxml);

        if( $fileSaveResult === FALSE )
            $resultMessage = "Failure writing sitemap.xml to $fileLocation!";
        else
            $resultMessage = "Success! sitemap.xml written to $fileLocation.";

        $mgClient = new Mailgun('key-43c92a7259f73ab1bda5ab3e0d7733c4');
        $mgClient->sendMessage('answeredby.com', array(
            'from'    => 'AnsweredBy <' . $this->getContainer()->getParameter('mailer_user') . '>',
            'to'      => 'pat.zabawa@answeredby.com',
            'subject' => 'sitemap-news:create Result',
            'text'    => $resultMessage
        ));

        $output->writeln($resultMessage);
    }
}