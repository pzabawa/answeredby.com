<?php

namespace Answeredby\SharedBundle\Helper;

class TwigGlobalsHelper
{
    private $doctrine;
    private $securityContext;

    public function __construct(\Doctrine\Bundle\DoctrineBundle\Registry $doctrine, \Symfony\Component\Security\Core\SecurityContext $securityContext)
    {
        $this->doctrine = $doctrine;
        $this->securityContext = $securityContext;
    }

    protected function _getQueryBase($limit)
    {
        $notificationsQuery = $this->doctrine
                ->getRepository('AnsweredbyEntityBundle:Notification')
                ->createQueryBuilder('notification')
                ->innerJoin('notification.destination_user', 'notificationDestinationUser')
                ->where('notificationDestinationUser = :currentUser')
                ->andWhere('notification.new_postshare IS NULL')
                ->setParameter('currentUser', $this->securityContext->getToken()->getUser())
                ->orderBy('notification.createdDatetime', 'DESC');
        if( $limit )
            $notificationsQuery = $notificationsQuery->setMaxResults($limit);
        return $notificationsQuery;
    }

    public function getUsersUnviewedNotifications($limit = NULL)
    {
        return $this->_getQueryBase($limit)
                ->andWhere('notification.viewed = 0')
                ->getQuery()
                ->getResult();
    }

    public function getUsersNotifications($limit = NULL)
    {
        return $this->_getQueryBase($limit)
                ->getQuery()
                ->getResult();
    }

    public function getUsersCurrentUserMayKnow()
    {
        $usersCurrentUserMayKnowResults = $this->doctrine->getManager()
                ->createQuery('SELECT user AS userObject, COUNT(user.id) AS userIdCount '
                            . 'FROM AnsweredbyEntityBundle:User user '
                        // User's relationships
                            . 'INNER JOIN AnsweredbyEntityBundle:Relationship usersRelationships WITH (usersRelationships.originationUser = user OR usersRelationships.destination_user = user) '
                        // User's relationships' relationships
                            . 'INNER JOIN AnsweredbyEntityBundle:Relationship currentUsersRelationships WITH ( '
                            .     'currentUsersRelationships.destination_user = usersRelationships.destination_user '
                            .     'OR currentUsersRelationships.destination_user = usersRelationships.originationUser '
                            .     'OR currentUsersRelationships.originationUser = usersRelationships.destination_user '
                            .     'OR currentUsersRelationships.originationUser = usersRelationships.originationUser '
                            . ') '
                            . 'WHERE (currentUsersRelationships.originationUser = :currentUser OR currentUsersRelationships.destination_user = :currentUser) '
                        // Users without a relationship with the currentUser
                            . 'AND user NOT IN ( '
                            .     'SELECT u2 FROM AnsweredbyEntityBundle:User u2 '
                            .     'INNER JOIN AnsweredbyEntityBundle:Relationship r2 WITH (r2.originationUser = u2 OR r2.destination_user = u2) '
                            .     'WHERE (r2.originationUser = :currentUser OR r2.destination_user = :currentUser) '
                            . ') '
                            . 'AND user NOT IN ( '
                            .     'SELECT u3 FROM AnsweredbyEntityBundle:User u3 '
                            .     'INNER JOIN AnsweredbyEntityBundle:UserSuggestionHide userSuggestionHide WITH userSuggestionHide.destination_user = u3 '
                            .     'WHERE userSuggestionHide.createdUser = :currentUser '
                            . ') '
                            . 'AND user.privacySetting <= 2 '
                            . 'AND user != :currentUser '
                            . 'GROUP BY user.id '
                            . 'ORDER BY userIdCount')
                ->setParameter('currentUser', $this->securityContext->getToken()->getUser())
                ->getResult();

        $usersCurrentUserMayKnow = array();
        foreach( $usersCurrentUserMayKnowResults AS $usersCurrentUserMayKnowResult ){
            $usersCurrentUserMayKnow[] = $usersCurrentUserMayKnowResult['userObject'];
        }

        return $usersCurrentUserMayKnow;
    }

    public function getRelationshipsAwaitingCurrentUsersApproval($limit = 0)
    {
        $relationshipsAwaitingCurrentUsersApprovalQuery = $this->doctrine
                ->getRepository('AnsweredbyEntityBundle:Relationship')
                ->createQueryBuilder('relationship')
                ->where('relationship.createdUser != :currentUser')
                ->andWhere('(relationship.originationUser = :currentUser OR relationship.destination_user = :currentUser) ')
                ->andWhere('NOT (relationship.approvedByOriginationUser = 1 AND relationship.approvedByDestinationUser = 1)')
                ->andWhere('relationship.ignoredByNonCreatedUser = 0')
                ->setParameter('currentUser', $this->securityContext->getToken()->getUser())
                ->orderBy('relationship.createdDatetime', 'DESC');
        if( $limit )
            $relationshipsAwaitingCurrentUsersApprovalQuery = $relationshipsAwaitingCurrentUsersApprovalQuery->setMaxResults($limit);
        return $relationshipsAwaitingCurrentUsersApprovalQuery
                ->getQuery()
                ->getResult();
    }

    public function getUsersCurrentUserHasRelationshipsWith()
    {
        return $this->doctrine
            ->getRepository('AnsweredbyEntityBundle:User')
            ->createQueryBuilder('user')
            ->innerJoin('AnsweredbyEntityBundle:Relationship', 'relationship')
            ->where('(relationship.originationUser = user OR relationship.destination_user = user) ')
            ->andWhere('(relationship.originationUser = :currentUser OR relationship.destination_user = :currentUser) ')
            ->andWhere('user != :currentUser')
            ->setParameter('currentUser', $this->securityContext->getToken()->getUser())
            ->getQuery()
            ->getResult();
    }
}
