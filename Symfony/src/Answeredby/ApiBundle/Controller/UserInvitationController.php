<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class UserInvitationController extends Controller
{
    public function postAction()
    {
        if( empty($_POST) )
            return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));

        $entityManager = $this->getDoctrine()->getManager();

        $invitation = $this->getUser()->getInvitation();
        if( !$invitation )
            return new Response(json_encode(array('error' => 'No invitation found for user.')), 400, array('Content-Type'=>'application/json'));

        $userInvitation = $entityManager->find('Answeredby\EntityBundle\Entity\UserInvitation', array('createdUser' => filter_input(INPUT_POST, 'created_user_id', FILTER_SANITIZE_NUMBER_INT), 'invitation' => $invitation->getId()));
        if( !$invitation )
            return new Response(json_encode(array('error' => 'No user invitation found between users.')), 400, array('Content-Type'=>'application/json'));

        $userInvitation->setIsHiddenByInvitationDestinationUser(TRUE);

        $entityManager->persist($userInvitation);
        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }
}
