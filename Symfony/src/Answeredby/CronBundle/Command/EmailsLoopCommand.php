<?php

// To run:
// $ app/console emails:loop

namespace Answeredby\CronBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mailgun\Mailgun;

class EmailsLoopCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('emails:loop')->setDescription('Loop e-mail manager, sending out relevant e-mails every second.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $tmpIsRunningFileName = '/tmp/emails_loop_is_running';

        // Create the script's log file.
        $tmpLogFileName = '/tmp/emails_loop_' . time() . '.log';
        touch($tmpLogFileName);

        // While the log file exists, keep checking for and sending out e-mails.
        while( file_exists($tmpLogFileName) ){

            // Make sure another script isn't running.
            if( !file_exists($tmpIsRunningFileName) ){

                // Mark this script as running and start a new log file.
                touch($tmpIsRunningFileName);
                $tmpFileHandle = fopen($tmpLogFileName, 'w');
                fwrite($tmpFileHandle, date('D M j Y, G:i:s T') . "\n");

                // Send the e-mails.
                $this->_invitationEmailsSend($tmpFileHandle);
                $this->_notificationEmailsSend($tmpFileHandle);
                $this->_relationshipEmailsSend($tmpFileHandle);

                // Close the log and "is running" files.
                fclose($tmpFileHandle);
                unlink($tmpIsRunningFileName);
            }
            sleep(1);
        }

        $output->writeln('emails:loop done!');
    }

    private function _invitationEmailsSend(&$tmpFileHandle)
    {
        $userInvitations = $this->getContainer()->get('doctrine')
                ->getRepository('AnsweredbyEntityBundle:UserInvitation')
                ->createQueryBuilder('userInvitation')
                ->where('userInvitation.wasEmailSent = 0')
                ->getQuery()
                ->getResult();

        // Create an e-mail for each Invitation.
        $mgClient = new Mailgun('key-43c92a7259f73ab1bda5ab3e0d7733c4');
        foreach( $userInvitations AS $userInvitation ){
            if( $userInvitation->getInvitation()->getIsUnsubscribed() )
                continue;

            $mgClient->sendMessage('answeredby.com', array(
                'from'    => 'AnsweredBy <' . $this->getContainer()->getParameter('mailer_user') . '>',
                'to'      => $userInvitation->getInvitation()->getEmail(),
                'subject' => 'AnsweredBy Invitation',
                'text'    => $this->getContainer()->get('templating')->render('AnsweredbyEmailBundle:Invitation:email.txt.twig',  array('userInvitation' => $userInvitation)),
                'html'    => $this->getContainer()->get('templating')->render('AnsweredbyEmailBundle:Invitation:email.html.twig', array('userInvitation' => $userInvitation))
            ));
        }

        // Set the Invitations as e-mail sent.
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        foreach( $userInvitations AS $userInvitation ){
            $userInvitation->setWasEmailSent(TRUE);
            $entityManager->persist($userInvitation);
        }

        try {
            $entityManager->flush();
            fwrite($tmpFileHandle, "Invitations sent!\n");
        } catch (\Doctrine\DBAL\DBALException $e) {
            fwrite($tmpFileHandle, "Invitations failed to save...\n");
        }
    }

    private function _notificationEmailsSend(&$tmpFileHandle)
    {
        $notifications = $this->getContainer()->get('doctrine')
                ->getRepository('AnsweredbyEntityBundle:Notification')
                ->createQueryBuilder('notification')
                ->where('notification.wasEmailSent = 0')
                ->getQuery()
                ->getResult();

        // Create an e-mail for each Notification.
        $mgClient = new Mailgun('key-43c92a7259f73ab1bda5ab3e0d7733c4');
        foreach( $notifications AS $notification ){
            if( ($notification->getNewSubpost()   && !$notification->getDestinationUser()->getIsEmailNotificationSubpostsSet())
             || ($notification->getNewPostshare() && !$notification->getDestinationUser()->getIsEmailNotificationSharesSet())
             || ($notification->getNewUserpost()  && !$notification->getDestinationUser()->getIsEmailNotificationUpvotesSet()) 
             || !$notification->getTargetPost() )
                continue;

            $mgClient->sendMessage('answeredby.com', array(
                'from'    => 'AnsweredBy <' . $this->getContainer()->getParameter('mailer_user') . '>',
                'to'      => $notification->getDestinationUser()->getEmail(),
                'subject' => 'AnsweredBy Notification',
                'text'    => $this->getContainer()->get('templating')->render('AnsweredbyEmailBundle:Notification:email.txt.twig',  array('notification' => $notification)),
                'html'    => $this->getContainer()->get('templating')->render('AnsweredbyEmailBundle:Notification:email.html.twig', array('notification' => $notification))
            ));
        }

        // Set the Invitations as e-mail sent.
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        foreach( $notifications AS $notification ){
            $notification->setWasEmailSent(TRUE);
            $entityManager->persist($notification);
        }

        try {
            $entityManager->flush();
            fwrite($tmpFileHandle, "Notification e-mails sent!\n");
        } catch (\Doctrine\DBAL\DBALException $e) {
            fwrite($tmpFileHandle, "Notifications failed to save...\n");
        }
    }

    private function _relationshipEmailsSend(&$tmpFileHandle)
    {
        $relationships = $this->getContainer()->get('doctrine')
                ->getRepository('AnsweredbyEntityBundle:Relationship')
                ->createQueryBuilder('relationship')
                ->where('relationship.wasEmailSent = 0')
                ->andWhere('relationship.ignoredByNonCreatedUser = 0')
                ->andWhere('relationship.createdDatetime <= :oneMinuteAgo')
                ->setParameter('oneMinuteAgo', new \DateTime('-1 minute'))
                ->getQuery()
                ->getResult();

        // Get the recipients from the new Relationships.
        $recipients = array();
        foreach( $relationships AS $relationship ){
            if( $relationship->getApprovedByOriginationUser() && $relationship->getApprovedByDestinationUser() )
                continue;

            if( !$relationship->getNonCreatedUser()->getIsEmailNotificationRelationshipRequestsSet() )
                continue;

            $recipients[$relationship->getNonCreatedUser()->getEmail()][] = $relationship;
        }

        // Create an e-mail for each recipient.
        $mgClient = new Mailgun('key-43c92a7259f73ab1bda5ab3e0d7733c4');
        foreach( $recipients AS $emailAddress => $relationshipsArray ){
            $mgClient->sendMessage('answeredby.com', array(
                'from'    => 'AnsweredBy <' . $this->getContainer()->getParameter('mailer_user') . '>',
                'to'      => $emailAddress,
                'subject' => 'AnsweredBy Relationship Requests',
                'text'    => $this->getContainer()->get('templating')->render('AnsweredbyEmailBundle:Relationship:email.txt.twig',  array('relationships' => $relationshipsArray)),
                'html'    => $this->getContainer()->get('templating')->render('AnsweredbyEmailBundle:Relationship:email.html.twig', array('relationships' => $relationshipsArray))
            ));
        }

        // Set the Relationship Requests as e-mail sent.
        $entityManager = $this->getContainer()->get('doctrine')->getEntityManager();
        foreach( $relationships AS $relationship ){
            $relationship->setWasEmailSent(TRUE);
            $entityManager->persist($relationship);
        }

        try {
            $entityManager->flush();
            fwrite($tmpFileHandle, "Relationship request e-mails sent!\n");
        } catch (\Doctrine\DBAL\DBALException $e) {
            fwrite($tmpFileHandle, "Relationships failed to save...\n");
        }  
    }
}
