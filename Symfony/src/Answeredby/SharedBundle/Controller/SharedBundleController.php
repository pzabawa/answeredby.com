<?php

namespace Answeredby\SharedBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Answeredby\EntityBundle\Entity\User;
use Answeredby\EntityBundle\Entity\Relationship;
use Answeredby\EntityBundle\Entity\PostShareDestinationUser;
use Answeredby\EntityBundle\Entity\Notification;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\True;

class SharedBundleController extends Controller
{
    protected function _isPostAccessibleByCurrentUser($post)
    {
        $postsQuery = $this->getDoctrine()
                ->getRepository('AnsweredbyEntityBundle:Post')
                ->createQueryBuilder('post');

        $postsQueryWhere = $postsQuery->expr()->andx();
        $postsQueryWhere->add($postsQuery->expr()->eq('post', ':post'));
        $postsQuery->setParameter('post', $post);

        // If logged in, show:
        //  1. Posts shared with you -- if you've been approved
        //  2. Public Posts
        //  3. Your Posts
        if( $this->getUser() ){
            $postsQuery = $postsQuery   
                    ->leftJoin('post.postShares','postShares')
                    ->leftJoin('postShares.networks','networks')
                    ->leftJoin('networks.relationships','relationships')
                    ->leftJoin('postShares.postShareDestinationUsers', 'postShareDestinationUsers')
                    ->setParameter('currentUser', $this->getUser());
            $postsQueryWhere->add(
                $postsQuery->expr()->orx(
                    $postsQuery->expr()->andx(
                        'postShareDestinationUsers.destinationUser = :currentUser',
                        'networks.isIndividualSharesType = 1',
                        'relationships.approvedByOriginationUser = 1',
                        'relationships.approvedByDestinationUser = 1'
                    ),
                    $postsQuery->expr()->andx(
                        'networks.isIndividualSharesType = 0',
                        'relationships.approvedByOriginationUser = 1',
                        'relationships.approvedByDestinationUser = 1',
                        'relationships.originationUser = post.createdUser',
                        'relationships.destination_user = :currentUser'
                    ),
                    'post.is_public = 1',
                    'post.createdUser = :currentUser'
                )
            );
        // Else only show public Posts
        } else {
            $postsQueryWhere->add($postsQuery->expr()->eq('post.is_public', '1'));
        }
        $posts = $postsQuery
                ->where($postsQueryWhere)
                ->orderBy('post.createdDatetime', 'DESC')
                ->getQuery()
                ->getResult();

        return count($posts) ? TRUE : FALSE;
    }

    protected function _getUserByUsernameSearch($usernameSearch, $maxResultCount=50)
    {
        return $this->getDoctrine()->getManager()
                ->createQuery('SELECT DISTINCT user '
                    . 'FROM AnsweredbyEntityBundle:User user '
                // User's relationships
                    . 'LEFT JOIN AnsweredbyEntityBundle:Relationship usersRelationships WITH (usersRelationships.originationUser = user OR usersRelationships.destination_user = user) '
                // User's relationships' relationships
                    . 'LEFT JOIN AnsweredbyEntityBundle:Relationship currentUsersRelationships WITH ( '
                    .     'currentUsersRelationships.destination_user = usersRelationships.destination_user '
                    .     'OR currentUsersRelationships.destination_user = usersRelationships.originationUser '
                    .     'OR currentUsersRelationships.originationUser = usersRelationships.destination_user '
                    .     'OR currentUsersRelationships.originationUser = usersRelationships.originationUser '
                    . ') '
                    . 'WHERE ('
                    . 'user.privacySetting = 3 '
                    . 'OR (user.privacySetting = 2 AND (currentUsersRelationships.originationUser = :currentUser OR currentUsersRelationships.destination_user = :currentUser)) '
                    . 'OR (user.privacySetting = 1 AND (usersRelationships.originationUser = :currentUser OR usersRelationships.destination_user = :currentUser)) '
                    . ')'
                    . 'AND user != :currentUser '
                    . 'AND user.username LIKE :searchString')
                ->setParameter('searchString', $usernameSearch)
                ->setParameter('currentUser', $this->getUser())
                ->setMaxResults($maxResultCount)
                ->getResult();
    }

    protected function _loginAction($twigTemplate, $templateOptions = array())
    {
        $request = $this->getRequest();
        $session = $request->getSession();
        $registrationErrors = array();

        // Get the login error if there is one.
        if ($request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
        }

        // Build the necessary variables.
        $user = new User();
        $target_url = $this->get('router')->generate('HomePage', array(), TRUE);
        $target_fragment = "";
        if( $session->has('target_url') ){
            $target_url = $session->get('target_url');
        }
        if( $session->has('target_fragment') ){
            $target_fragment = $session->get('target_fragment');
        }
        if( $request->query->get('target_url') ){
            $target_url = filter_var($request->query->get('target_url'), FILTER_SANITIZE_STRING);
            $session->set('target_url', $target_url);
        }
        if( $request->query->get('target_fragment') ){
            $target_fragment = filter_var($request->query->get('target_fragment'), FILTER_SANITIZE_STRING);
            $session->set('target_fragment', $target_fragment);
        }
        $form = $this->createFormBuilder($user)
                ->add('username', 'text', array('label' => 'Display Name', 'max_length' => 25))
                ->add('password', 'password', array('label' => 'Password', 'max_length' => 255))
                ->add('email', 'email', array('label' => 'E-mail Address', 'max_length' => 255))
                ->add('birthday', 'birthday', array('years' => range(date('Y'), date('Y') - 120)))
                ->add('recaptcha', 'ewz_recaptcha', array(
                    'label' => 'Verify Humanity',
                    'attr'          => array(
                        'options' => array(
                            'theme' => 'clean'
                        )
                    ),
                    'mapped' => false,
                    'constraints'   => array(
                        new True()
                    )
                ))
                ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            if ($form->isValid()) {

                // Check that the user is 13 years old.
                if( $user->getBirthday()->diff(new \DateTime())->format('%y') < 13 ){
                    return $this->render($twigTemplate, array_merge($templateOptions, 
                            array(
                                'last_username'      => $session->get(SecurityContext::LAST_USERNAME),  // Last username entered by the user.
                                'error'              => $error,
                                'registrationErrors' => array('You must be 13 years old to register.'),
                                'form'               => $form->createView(),
                                'target_url'         => $target_url,
                                'target_fragment'    => $target_fragment
                            )));
                }

                // Encrypt the password.
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder($user);
                $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                $user->setPassword($password);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                try {
                    $entityManager->flush();

                    // Assign Invitations/Networks to this User's e-mail address to this User.
                    $invitation = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Invitation')->findOneByEmail($user->getEmail());
                    if( $invitation ){
                        $user->setInvitation($invitation);
                        $invitation->setDestinationUser($user);
                        $entityManager->persist($invitation);

                        foreach( $invitation->getUserInvitations() AS $userInvitation ){
                            foreach( $userInvitation->getNetworks() AS $network ){
                                // If any Invitation has invited the User to a Network, show that Tutorial page.
                                $user->setShowTodoPage1(TRUE);
                                $relationship = new Relationship($network->getCreatedUser(), $network->getCreatedUser(), $user, $network);
                                $relationship->setApprovedByOriginationUser(TRUE);
                                $entityManager->persist($relationship);
                            }
                        }

                        $entityManager->persist($user);
                    }

                    try {
                        $entityManager->flush();
                    } catch (\Doctrine\DBAL\DBALException $e) {
                        return $this->render($twigTemplate, array_merge($templateOptions,
                            array(
                                'last_username'      => $session->get(SecurityContext::LAST_USERNAME),  // Last username entered by the user.
                                'error'              => $error,
                                'registrationErrors' => array('Your friends have invited you to one too many Networks... Try again?'),
                                'form'               => $form->createView(),
                                'target_url'         => $target_url,
                                'target_fragment'    => $target_fragment
                            )));
                    }
                } catch (\Doctrine\DBAL\DBALException $e) {
                    if( $e->getPrevious()->getCode() == 23000 ){
                        // Check for duplicate username.
                        if( $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->findOneByUsername($user->getUsername()) )
                            $registrationErrors[] .= "Display Name in use &mdash; Duplicates aren't allowed.";
                        // Check for duplicate e-mail address.
                        if( $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->findOneByEmail($user->getEmail()) )
                            $registrationErrors[] .= "There is already an account associated with that e-mail address. If you've forgotten your username or password, visit the <a href='" . $this->get('router')->generate('ForgotPage') . "'>\"I forgot something!\"</a> page.";
                        // Send them back with error message.
                        if( $registrationErrors ){
                            return $this->render($twigTemplate, array_merge($templateOptions,
                                    array(
                                        'last_username'      => $session->get(SecurityContext::LAST_USERNAME),  // Last username entered by the user.
                                        'error'              => $error,
                                        'registrationErrors' => $registrationErrors,
                                        'form'               => $form->createView(),
                                        'target_url'         => $target_url,
                                        'target_fragment'    => $target_fragment
                                    )));
                        }
                    }
                    throw $e;
                }

                // Log the user in.
                $token = new UsernamePasswordToken($user, null, 'secured_area', $user->getRoles());
                $this->get('security.context')->setToken($token);
                $this->get('session')->set('_security_secured_area',serialize($token));

                // Remove the session targets used only for this page.
                $session->remove('target_url');
                $session->remove('target_fragment');

                // Forward the user to the requested page.
                $session->set('first_time_to_homepage', TRUE);
                return $this->redirect($this->get('router')->generate('HomePage'));
            }
        }

        // Prevent brute force attacks by delaying each failed login 1 second.
        if( $error )
            sleep(1);

        return $this->render($twigTemplate, array_merge($templateOptions,
                array(
                    'last_username'      => $session->get(SecurityContext::LAST_USERNAME),  // Last username entered by the user.
                    'error'              => $error,
                    'registrationErrors' => $registrationErrors,
                    'form'               => $form->createView(),
                    'target_url'         => $target_url,
                    'target_fragment'    => $target_fragment
                )));
    }

    public function _canCurrentUserAccessUserPageOfUser(\Answeredby\EntityBundle\Entity\User $user)
    {
        // Allow access if the User allows public access...
        if( $user->getPrivacySetting() == 3 ){
            return TRUE;
        }

        // If the User is the current User, allow access.
        if( $this->getUser() == $user )
            return TRUE;

        if( $this->getUser() && $user->getPrivacySetting() == 2 ){
            $isUserFriendOfFriend = count(
                    $this->getDoctrine()->getManager()
                        ->createQuery('SELECT user '
                                . 'FROM AnsweredbyEntityBundle:User user '
                            // User's relationships
                                . 'INNER JOIN AnsweredbyEntityBundle:Relationship usersRelationships WITH (usersRelationships.originationUser = user OR usersRelationships.destination_user = user) '
                            // User's relationships' relationships
                                . 'INNER JOIN AnsweredbyEntityBundle:Relationship currentUsersRelationships WITH ( '
                                .     'currentUsersRelationships.destination_user = usersRelationships.destination_user '
                                .     'OR currentUsersRelationships.destination_user = usersRelationships.originationUser '
                                .     'OR currentUsersRelationships.originationUser = usersRelationships.destination_user '
                                .     'OR currentUsersRelationships.originationUser = usersRelationships.originationUser '
                                . ') '
                                . 'WHERE (currentUsersRelationships.originationUser = :currentUser OR currentUsersRelationships.destination_user = :currentUser) '
                                . 'AND user = :profilePageUser')
                        ->setParameter('currentUser', $this->getUser())
                        ->setParameter('profilePageUser', $user)
                        ->getResult());
            return $isUserFriendOfFriend;
        }

        if( $this->getUser() && $user->getPrivacySetting() == 1 ){
            $isUserFriend = count(
                    $this->getDoctrine()->getManager()
                        ->createQuery('SELECT user '
                                . 'FROM AnsweredbyEntityBundle:User user '
                            // User's relationships
                                . 'INNER JOIN AnsweredbyEntityBundle:Relationship usersRelationships WITH (usersRelationships.originationUser = user OR usersRelationships.destination_user = user) '
                                . 'WHERE (usersRelationships.originationUser = :currentUser OR usersRelationships.destination_user = :currentUser) '
                                . 'AND user = :profilePageUser')
                        ->setParameter('currentUser', $this->getUser())
                        ->setParameter('profilePageUser', $user)
                        ->getResult());
            return $isUserFriend;
        }

        return FALSE;
    }

    protected function _processPostSharePost(&$postShare)
    {
        $arrayOfUsersToNotify = array();
        $postShareNetworkIds = filter_input(INPUT_POST, 'network_array', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
        if( !empty($postShareNetworkIds) ){
            foreach( $postShareNetworkIds AS $postShareNetworkId ){
                $network = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Network')->find($postShareNetworkId);
                if( $network->getCreatedUser() != $this->getUser() )
                    continue;
                $postShare->addNetwork($network);
                foreach( $network->getRelationships() AS $networkRelationship ){
                    if( $networkRelationship->getApprovedByOriginationUser()
                     && $networkRelationship->getApprovedByDestinationUser() )
                        $arrayOfUsersToNotify[$networkRelationship->getDestinationUser()->getId()] = TRUE;
                }
            }
        }
        $this->getUser()->setSerializedDefaultPostShareNetworkIds(serialize($postShareNetworkIds));

        $individualUsernames = filter_input(INPUT_POST, 'friends_array', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
        if( !empty($individualUsernames) ){
            $currentUsersIndividualSharesNetwork = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Network')->findOneBy(array('createdUser' => $this->getUser(), 'isIndividualSharesType' => TRUE));
            $postShare->addNetwork($currentUsersIndividualSharesNetwork);
            foreach( $individualUsernames AS $individualUsername ){
                $destinationUser = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->findOneByUsername($individualUsername);
                // If no destinationUser, throw an exception.
                if( !$destinationUser )
                    throw new \Exception("User \"$individualUsername\" doesn't exist. Perhaps they changed their Display Name?");
                $postShare->addPostShareDestinationUser(new PostShareDestinationUser($postShare, $destinationUser));
                // If $currentUsersIndividualSharesNetwork has a Relationship with the Destination User and it's approved by both sides.
                if( count($currentUsersIndividualSharesNetwork->getApprovedRelationshipsWithDestinationUser($destinationUser)) )
                    $arrayOfUsersToNotify[$destinationUser->getId()] = TRUE;
            }
        }
        $this->getUser()->setSerializedDefaultPostShareFriendUsernames(serialize($individualUsernames));

        $entityManager = $this->getDoctrine()->getManager();
        foreach( $arrayOfUsersToNotify AS $destinationUserId => $true ){
            $notification = new Notification($this->getUser(), $entityManager->getReference('AnsweredbyEntityBundle:User', $destinationUserId));
            $notification->setNewPostshare($postShare);
            $postShare->addNotification($notification);
        }

        $entityManager->persist($this->getUser());
        $entityManager->persist($postShare);
        $entityManager->flush();
    }
}
