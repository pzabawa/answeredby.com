<?php

namespace Answeredby\ApiBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\Post;
use Answeredby\EntityBundle\Entity\PostShare;
use Answeredby\EntityBundle\Entity\Photo;

class PostController extends SharedBundleController
{
    private function _buildPostStdClassFrom($postObject)
    {
        $postStdClass = new \stdClass();
        $postStdClass->id = $postObject->getId();
        $postStdClass->title = $postObject->getTitle();
        $postStdClass->text = $postObject->getText();
        $postStdClass->hiddenText = $postObject->getHiddenText() ? $postObject->getHiddenText() : "";
        $postStdClass->createdDatetime = $postObject->getCreatedDatetime()->format('Y-m-d\\TH:i:sP');
        $postStdClass->createdUserId = $postObject->getCreatedUser()->getId();
        $postStdClass->createdUserUsername = $postObject->getCreatedUser()->getUsername();
        $postStdClass->createdUserImageUrl = $postObject->getCreatedUser()->getProfilePicUrl();
        $postStdClass->createdUserBackgroundHtmlColor = $postObject->getCreatedUser()->getBackgroundHtmlColor();
        $postStdClass->createdUserUrl = $this->generateUrl('UserPage', array('userid' => $postObject->getCreatedUser()->getId(), 'username' => $postObject->getCreatedUser()->getUrlUsername()));
        $postStdClass->hasParentPost = $postObject->getParentPost() ? TRUE : FALSE;
        if( !$postObject->getParentPost() && $this->getUser() == $postObject->getCreatedUser() ){
            $postStdClass->currentUserCreatedPost = TRUE;
            $postStdClass->isPublic = $postObject->getIsPublic() ? TRUE : FALSE;
        }
        if( $postObject->getAttachmentIsShown() ){
            $postStdClass->attachmentTitle = $postObject->getAttachmentTitle();
            $postStdClass->attachmentDescription = $postObject->getAttachmentDescription();
            $postStdClass->attachmentUrl = $postObject->getAttachmentUrl();
            $postStdClass->attachmentUrlHost = parse_url($postObject->getAttachmentUrl()) ? parse_url($postObject->getAttachmentUrl()) : '';
            $postStdClass->attachmentImageUrl = $postObject->getAttachmentImageUrl();
            $postStdClass->attachmentVideoUrl = $postObject->getAttachmentVideoUrl();
            $postStdClass->attachmentVideoHeight = $postObject->getAttachmentVideoHeight();
            $postStdClass->attachmentVideoWidth = $postObject->getAttachmentVideoWidth();
        }
        $postStdClass->photos = array();
        foreach( $postObject->getPhotos() AS $photo )
            $postStdClass->photos[] = $photo->getUrl();
        $postStdClass->userPostsCount = count($postObject->getUserPosts());
        $postStdClass->userPostsCreatedByUser = count($postObject->getUserPostsCreatedBy($this->getUser()));
        $postStdClass->activePostShares = count($postObject->getPostShares());
        $postStdClass->activePostSharesCreatedByUser = count($postObject->getPostSharesCreatedBy($this->getUser()));
        $postStdClass->subpostCount = count($postObject->getChildrenPosts());
        return $postStdClass;
    }

    protected function _getStdClassPostSharesForCurrentUserAndPost(\Answeredby\EntityBundle\Entity\Post $post)
    {
        // Get the active PostShares created for the current user.
        $postShares = $this->getDoctrine()
                ->getRepository('AnsweredbyEntityBundle:PostShare')
                ->createQueryBuilder('postShare');
        $postShares = $postShares
                ->where('postShare.post = :currentPost')
                ->setParameter('currentPost', $post)
                ->leftJoin('postShare.networks','networks')
                ->leftJoin('networks.relationships','relationships')
                ->leftJoin('postShare.postShareDestinationUsers', 'postShareDestinationUsers')
                ->andWhere(
                    $postShares->expr()->orx(
                        $postShares->expr()->andx(
                            'postShareDestinationUsers.destinationUser = :currentUser',
                            'networks.isIndividualSharesType = 1',
                            'relationships.approvedByOriginationUser = 1',
                            'relationships.approvedByDestinationUser = 1'
                        ),
                        $postShares->expr()->andx(
                            'networks.isIndividualSharesType = 0',
                            'relationships.approvedByOriginationUser = 1',
                            'relationships.approvedByDestinationUser = 1',
                            'relationships.originationUser = :postCreatedUser',
                            'relationships.destination_user = :currentUser'
                        )
                    )
                )
                ->setParameter('currentUser', $this->getUser())
                ->setParameter('postCreatedUser', $post->getCreatedUser())
                ->orderBy('postShare.createdDatetime', 'DESC')
                ->getQuery()
                ->getResult();
        $postShareStdClasses = array();
        foreach( $postShares AS $postShare ){
            $postShareStdClass = new \stdClass();
            $postShareStdClass->id = $postShare->getId();
            $postShareStdClass->message = $postShare->getMessage();
            $postShareStdClass->createdDatetime = $postShare->getCreatedDatetime()->format('Y-m-d\\TH:i:sP');
            $postShareStdClass->createdUserId = $postShare->getCreatedUser()->getId();
            $postShareStdClass->createdUserUsername = $postShare->getCreatedUser()->getUsername();
            $postShareStdClass->createdUserBackgroundColor = $postShare->getCreatedUser()->getBackgroundHtmlColor();
            $postShareStdClass->createdUserImageUrl = $postShare->getCreatedUser()->getProfilePicUrl();
            $postShareStdClass->createdUserUrl = $this->generateUrl('UserPage', array('userid' => $postShare->getCreatedUser()->getId(), 'username' => $postShare->getCreatedUser()->getUrlUsername()));
            $postShareStdClass->networks = array();
            foreach( $postShare->getNetworks() AS $postShareDestinationUserNetwork ){
                $networkStdClass = new \stdClass();
                $networkStdClass->id = $postShareDestinationUserNetwork->getId();
                $networkStdClass->text = $postShareDestinationUserNetwork->getText();
                $networkStdClass->approvedByDestinationUser = count($postShareDestinationUserNetwork->getDestinationUserApprovedRelationshipWithUsers($postShare->getCreatedUser(), $this->getUser()));
                $postShareStdClass->networks[] = $networkStdClass;
            }
            $postShareStdClasses[] = $postShareStdClass;
        }

        return $postShareStdClasses;
    }

    public function postAction()
    {
        if( !empty($_POST) ){
            $entityManager = $this->getDoctrine()->getManager();

            $post = new Post($this->getUser());
            $post->updateFromPostArray($_POST);
            $post->addAnchorTagsToTextUrls();
            $post->replaceBackslashNCharacatersWithBrTagsInText();
            $post->divideTextToTextAndHiddenText();

            $parentPostId = filter_input(INPUT_POST, 'parent_post', FILTER_SANITIZE_NUMBER_INT);
            if( $parentPostId ){
                $post->setParentPost($entityManager->getReference('AnsweredbyEntityBundle:Post', $parentPostId));
            }

            $post->constructNotifications();

            $photoUrlsToAttach = filter_input(INPUT_POST, 'arrayOfPhotosAttachedToCurrentNewPost', FILTER_SANITIZE_URL, FILTER_REQUIRE_ARRAY);
            if( $photoUrlsToAttach ){
                foreach( $photoUrlsToAttach AS $photoUrlToAttach ){
                    $photoToAttach = new Photo($photoUrlToAttach, $this->getUser());
                    $post->addPhoto($photoToAttach);
                    $photoToAttach->setPost($post);
                    $entityManager->persist($photoToAttach);
                }
            }

            $entityManager->persist($post);
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            if( !empty($_POST['network_array']) || !empty($_POST['friends_array']) ){
                $postShare = new PostShare($this->getUser());
                $postShare->setPost($post);
                $post->addPostShare($postShare);

                $entityManager->persist($postShare);
                try {
                    $entityManager->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
                }

                try {
                    $this->_processPostSharePost($postShare);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('message' => $e->getMessage())), 200, array('Content-Type'=>'application/json'));
                } catch (\Doctrine\DBAL\DBALException $e) {
                    return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
                }
            } else {
                $this->getUser()->setSerializedDefaultPostShareNetworkIds(serialize(array()));
                $this->getUser()->setSerializedDefaultPostShareFriendUsernames(serialize(array()));

                $entityManager->persist($this->getUser());
                try {
                    $entityManager->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
                }
            }

            return new Response(json_encode(array('post' => $this->_buildPostStdClassFrom($post))), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }

    public function subpostsAction($post_id)
    {
        $post = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Post')->find($post_id);

        if( !is_a($post, 'Answeredby\EntityBundle\Entity\Post') ){
            return new Response(json_encode(array('error' => "post_id $post_id not found")), 400, array('Content-Type'=>'application/json'));
        }

        // Make sure User can see the subposts' original Post.
        $parentPost = $post;
        while( $parentPost->getParentPost() )
            $parentPost = $parentPost->getParentPost();
        if( !$this->_isPostAccessibleByCurrentUser($parentPost) )
            return new Response(json_encode(array('subposts' => array())), 200, array('Content-Type'=>'application/json'));

        $subposts = new \stdClass();
        foreach( $post->getChildrenPosts() AS $childPost ){
            foreach( $childPost->getChildrenPosts() AS $childchildPost ){
                $subpost = $this->_buildPostStdClassFrom($childchildPost);
                $childPostId = $childPost->getId();
                if( empty($subposts->$childPostId) )
                    $subposts->$childPostId = array();
                array_push($subposts->$childPostId, $subpost);
            }
        }

        return new Response(json_encode(array('subposts' => $subposts)), 200, array('Content-Type'=>'application/json'));
    }

    public function homepageAction()
    {
        $postsQuery = $this->getDoctrine()
                ->getRepository('AnsweredbyEntityBundle:Post')
                ->createQueryBuilder('post');
        $postsQuery = $postsQuery
                ->innerJoin('post.postShares','postShares')
                ->leftJoin('postShares.networks','networks')
                ->leftJoin('networks.relationships','relationships')
                ->leftJoin('postShares.postShareDestinationUsers', 'postShareDestinationUsers')
                ->leftJoin('post.userPostHides', 'userPostHides')
                ->where(
                    $postsQuery->expr()->orx(
                        $postsQuery->expr()->andx(
                            'postShareDestinationUsers.destinationUser = :currentUser',
                            'networks.isIndividualSharesType = 1',
                            'relationships.approvedByOriginationUser = 1',
                            'relationships.approvedByDestinationUser = 1'
                        ),
                        $postsQuery->expr()->andx(
                            'networks.isIndividualSharesType = 0',
                            'relationships.approvedByOriginationUser = 1',
                            'relationships.approvedByDestinationUser = 1',
                            'relationships.originationUser = post.createdUser',
                            'relationships.destination_user = :currentUser'
                        )
                    )
                )
                ->andWhere('(userPostHides IS NULL OR userPostHides.createdUser != :currentUser)')
                ->setParameter('currentUser', $this->getUser());
        if( array_key_exists('starting_post_id', $_GET) ){
            $postsQuery = $postsQuery
                    ->andWhere('post.id < :starting_post_id')
                    ->setParameter('starting_post_id', filter_input(INPUT_GET, 'starting_post_id', FILTER_SANITIZE_NUMBER_INT));
        }

        $posts = $postsQuery
                ->orderBy('postShares.createdDatetime', 'DESC')
                ->setMaxResults(5)
                ->getQuery()
                ->getResult();

        $postStdClasses = array();
        foreach( $posts AS $post ){

            $childPosts = $post->getChildrenPosts();
            $childPostStdClasses = array();
            foreach( $childPosts AS $childPost ){
                $childPostStdClasses[] = $this->_buildPostStdClassFrom($childPost);
            }

            $postStdClass = $this->_buildPostStdClassFrom($post);
            $postStdClass->childposts = $childPostStdClasses;
            $postStdClass->postShares = $this->_getStdClassPostSharesForCurrentUserAndPost($post);
            $postStdClasses[] = $postStdClass;
        }

        return new Response(json_encode(array('posts' => $postStdClasses)), 200, array('Content-Type'=>'application/json'));
    }

    public function userpageAction($user_id)
    {
        $userObject = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->find($user_id);

        if( !$userObject )
            return new Response(json_encode(array('posts' => array())), 200, array('Content-Type'=>'application/json'));

        if( !$this->_canCurrentUserAccessUserPageOfUser($userObject) )
            return new Response(json_encode(array('posts' => array())), 200, array('Content-Type'=>'application/json'));

        // Show Posts created by user_id.
        // If user_id = currentUser, no additional filters.
        // Else:
        //  If logged in, show public posts + those shared with you -- if you've been approved.
        //  If not logged in, show just public posts.

        $postsQuery = $this->getDoctrine()
                ->getRepository('AnsweredbyEntityBundle:Post')
                ->createQueryBuilder('post');

        $postsQueryWhere = $postsQuery->expr()->andx();
        $postsQueryWhere->add('post.parentPost IS NULL');
        $postsQueryWhere->add($postsQuery->expr()->eq('post.createdUser', ':userId'));
        $postsQuery->setParameter('userId', $user_id);

        if( array_key_exists('starting_post_id', $_GET) ){
            $postsQueryWhere->add($postsQuery->expr()->lt('post.id', ':starting_post_id'));
            $postsQuery->setParameter('starting_post_id', filter_input(INPUT_GET, 'starting_post_id', FILTER_SANITIZE_NUMBER_INT));
        }
        if( $this->getUser() ){
            if( $this->getUser()->getId() != $user_id )
            {
                // If logged in, show public posts + those shared with you -- if you've been approved.
                $postsQuery = $postsQuery         
                        ->leftJoin('post.postShares','postShares')
                        ->leftJoin('postShares.networks','networks')
                        ->leftJoin('networks.relationships','relationships')
                        ->leftJoin('postShares.postShareDestinationUsers', 'postShareDestinationUsers')
                        ->setParameter('currentUser', $this->getUser());
                $postsQueryWhere->add(
                    $postsQuery->expr()->orx(
                        $postsQuery->expr()->andx(
                            'postShareDestinationUsers.destinationUser = :currentUser',
                            'networks.isIndividualSharesType = 1',
                            'relationships.approvedByOriginationUser = 1',
                            'relationships.approvedByDestinationUser = 1'
                        ),
                        $postsQuery->expr()->andx(
                            'networks.isIndividualSharesType = 0',
                            'relationships.approvedByOriginationUser = 1',
                            'relationships.approvedByDestinationUser = 1',
                            'relationships.originationUser = post.createdUser',
                            'relationships.destination_user = :currentUser'
                        ),
                        'post.is_public = 1'
                    )
                );
            }
        } else {
            $postsQueryWhere->add($postsQuery->expr()->eq('post.is_public', '1'));
        }

        $posts = $postsQuery
                ->where($postsQueryWhere)
                ->orderBy('post.createdDatetime', 'DESC')
                ->setMaxResults(5)
                ->getQuery()
                ->getResult();

        $postStdClasses = array();
        foreach( $posts AS $post ){
            $childPosts = $post->getChildrenPosts();
            $childPostStdClasses = array();
            foreach( $childPosts AS $childPost ){
                $childPostStdClasses[] = $this->_buildPostStdClassFrom($childPost);
            }
            $postStdClass = $this->_buildPostStdClassFrom($post);
            $postStdClass->childposts = $childPostStdClasses;
            $postStdClasses[] = $postStdClass;
        }

        return new Response(json_encode(array('posts' => $postStdClasses)), 200, array('Content-Type'=>'application/json'));
    }

    public function postpageAction($post_id)
    {
        $postObject = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Post')->find($post_id);

        if( !is_a($postObject, 'Answeredby\EntityBundle\Entity\Post') ){
            return new Response(json_encode(array('posts' => array())), 200, array('Content-Type'=>'application/json'));
        }

        if( $this->_isPostAccessibleByCurrentUser($postObject) ){
            $postStdClasses = array();
            $childPosts = $postObject->getChildrenPosts();
            $childPostStdClasses = array();
            foreach( $childPosts AS $childPost ){
                $childPostStdClasses[] = $this->_buildPostStdClassFrom($childPost);
            }
            $postStdClass = $this->_buildPostStdClassFrom($postObject);
            $postStdClass->childposts = $childPostStdClasses;
            $postStdClass->postShares = $this->_getStdClassPostSharesForCurrentUserAndPost($postObject);
            $postStdClasses[] = $postStdClass;

            return new Response(json_encode(array('posts' => $postStdClasses)), 200, array('Content-Type'=>'application/json'));
        }
        return new Response(json_encode(array('posts' => array())), 200, array('Content-Type'=>'application/json'));
    }
}
