<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="postShareDestinationUsers")
 */
class PostShareDestinationUser
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="PostShare", inversedBy="postShareDestinationUsers")
     * @ORM\JoinColumn(name="postShareId", referencedColumnName="id", nullable=false)
     */
    protected $postShare;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="postShareDestinationUsers")
     * @ORM\JoinColumn(name="destinationUserId", referencedColumnName="id", nullable=false)
     */
    protected $destinationUser;

    /**
     * @ORM\ManyToMany(targetEntity="Network")
     * @ORM\JoinTable(name="postShareDestinationUserNetworks",
     *      joinColumns={@ORM\JoinColumn(name="postShareId", referencedColumnName="postShareId"),
     *                   @ORM\JoinColumn(name="destinationUserId", referencedColumnName="destinationUserId")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="networkId", referencedColumnName="id")}
     *      )
     */
    protected $networks;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\PostShare $postShare, \Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        $this->_setPostShare($postShare);
        $this->_setDestinationUser($destinationUser);
        $this->networks = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set postShare
     *
     * @param \Answeredby\EntityBundle\Entity\PostShare $postShare
     * @return PostShareDestinationUser
     */
    protected function _setPostShare(\Answeredby\EntityBundle\Entity\PostShare $postShare)
    {
        $this->postShare = $postShare;

        return $this;
    }

    /**
     * Get postShare
     *
     * @return \Answeredby\EntityBundle\Entity\PostShare 
     */
    public function getPostShare()
    {
        return $this->postShare;
    }

    /**
     * Set destinationUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $destinationUser
     * @return PostShareDestinationUser
     */
    protected function _setDestinationUser(\Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        $this->destinationUser = $destinationUser;

        return $this;
    }

    /**
     * Get destinationUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getDestinationUser()
    {
        return $this->destinationUser;
    }

    /**
     * Add networks
     *
     * @param \Answeredby\EntityBundle\Entity\Network $networks
     * @return PostShareDestinationUser
     */
    public function addNetwork(\Answeredby\EntityBundle\Entity\Network $networks)
    {
        $this->networks[] = $networks;

        return $this;
    }

    /**
     * Remove networks
     *
     * @param \Answeredby\EntityBundle\Entity\Network $networks
     */
    public function removeNetwork(\Answeredby\EntityBundle\Entity\Network $networks)
    {
        $this->networks->removeElement($networks);
    }

    /**
     * Get networks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNetworks()
    {
        return $this->networks;
    }
}
