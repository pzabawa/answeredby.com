<?php

namespace Answeredby\ApiBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;
use Symfony\Component\HttpFoundation\Response;

class UserController extends SharedBundleController
{
    public function postEmailAction()
    {
        if( !empty($_POST) ){
            if( filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL) ){
                $newEmail = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

                // Check for duplicate e-mail address.
                if( $this->getUser()->getEmail() != $newEmail && $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->findOneByEmail($newEmail) )
                    return new Response(json_encode(array('success' => 0, 'duplicate_email' => 1)), 200, array('Content-Type'=>'application/json'));

                $this->getUser()->setEmail($newEmail);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($this->getUser());
                try {
                    $entityManager->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {
                    return new Response(json_encode(array('success' => 0, 'error' => $e->getPrevious()->getCode())), 200, array('Content-Type'=>'application/json'));
                }
                return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
            }
            return new Response(json_encode(array('success' => 0, 'invalid_email' => 1)), 200, array('Content-Type'=>'application/json'));
        }
        return new Response(json_encode(array('success' => 0)), 200, array('Content-Type'=>'application/json'));
    }

    public function postProfilePicAction()
    {
        if( !empty($_POST) ){
            $this->getUser()->setProfilePicUrl(filter_input(INPUT_POST, 'text', FILTER_SANITIZE_URL));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($this->getUser());
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }
            return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
        }
        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }

    public function autocompleteAction()
    {
        if( empty($_GET) || !isset($_GET['search_string']) )
            return new Response('[]', 200, array('Content-Type'=>'application/json'));

        $search_string = filter_var($_GET['search_string'], FILTER_SANITIZE_STRING);

        $results = $this->_getUserByUsernameSearch($search_string . '%', 10);
        if( !empty($results) )
            foreach( $results AS $result )
                $usernames[] = $result->getUsername();

        return new Response(json_encode($usernames), 200, array('Content-Type'=>'application/json'));
    }

    public function autocompleteRelationshipAction()
    {
        if( empty($_GET) || !isset($_GET['search_string']) )
            return new Response('[]', 200, array('Content-Type'=>'application/json'));

        $search_string = filter_var($_GET['search_string'], FILTER_SANITIZE_STRING);

        $query = $this->getDoctrine()->getManager()
                ->createQuery('SELECT user '
                    . 'FROM AnsweredbyEntityBundle:User user '
                // User's relationships
                    . 'INNER JOIN AnsweredbyEntityBundle:Relationship usersRelationships WITH (usersRelationships.originationUser = user OR usersRelationships.destination_user = user) '
                    . 'WHERE (usersRelationships.originationUser = :currentUser OR usersRelationships.destination_user = :currentUser) '
                    . 'AND user != :currentUser '
                    . 'AND user.username LIKE :searchString')
                ->setParameter('searchString', $search_string . '%')
                ->setParameter('currentUser', $this->getUser())
                ->setMaxResults(1);

        $usernames = array();
        $results = $query->getResult();
        if( !empty($results) )
            foreach( $results AS $result )
                $usernames[] = $result->getUsername();

        return new Response(json_encode($usernames), 200, array('Content-Type'=>'application/json'));
    }

    public function autocompleteApprovedRelationshipAction()
    {
        if( empty($_GET) || !isset($_GET['search_string']) )
            return new Response('[]', 200, array('Content-Type'=>'application/json'));

        $search_string = filter_var($_GET['search_string'], FILTER_SANITIZE_STRING);

        $query = $this->getDoctrine()->getManager()
            ->createQuery('SELECT user '
                    . 'FROM AnsweredbyEntityBundle:User user '
                    . 'INNER JOIN user.relationships_with_user relationship '
                    . 'INNER JOIN relationship.network network '
                    . 'WHERE relationship.originationUser = :currentUser '
                    . 'AND relationship.approvedByOriginationUser = 1 '
                    . 'AND relationship.approvedByDestinationUser = 1 '
                    . 'AND network.isIndividualSharesType = 1 '
                    . 'AND user.username LIKE :searchString')
            ->setParameter('searchString', $search_string.'%')
            ->setParameter('currentUser', $this->getUser())
            ->setMaxResults(1);

        $usernames = array();
        $results = $query->getResult();
        if( !empty($results) )
            foreach( $results AS $result )
                $usernames[] = $result->getUsername();

        return new Response(json_encode($usernames), 200, array('Content-Type'=>'application/json'));
    }

    public function postPrivacySettingAction()
    {
        if( empty($_POST) )
            return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));

        $this->getUser()->setPrivacySetting(filter_input(INPUT_POST, 'privacy_setting', FILTER_SANITIZE_NUMBER_INT));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($this->getUser());
        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }
        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }

    public function postEmailNotificationSettingAction()
    {
        if( empty($_POST) )
            return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));

        // Get the User,
        //  first checking the logged in User,
        //  then checking the destination user from the GET parameter notification_id/relationship_id
        //  (for e-mail subscription pages from e-mail notifications and relationship requests).
        $user = NULL;
        if( $this->getUser() ){
            $user = $this->getUser();
        } else if( array_key_exists('notification_id', $_GET) ){
            $notification = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Notification')->findOneByMd5(filter_input(INPUT_GET, 'notification_id', FILTER_SANITIZE_STRING));
            if( $notification )
                $user = $notification->getDestinationUser();
        } else if( array_key_exists('relationship_id', $_GET) ){
            $relationship = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Relationship')->findOneByMd5(filter_input(INPUT_GET, 'relationship_id', FILTER_SANITIZE_STRING));
            if( $relationship )
                $user = $relationship->getNonCreatedUser();
        }

        if( !$user )
            return new Response(json_encode(array('error' => 'No user found.')), 400, array('Content-Type'=>'application/json'));

        if( array_key_exists('relationship_requests', $_POST) )
            $user->setIsEmailNotificationRelationshipRequestsSet(filter_input(INPUT_POST, 'relationship_requests', FILTER_SANITIZE_NUMBER_INT));
        else if( array_key_exists('upvotes', $_POST) )
            $user->setIsEmailNotificationUpvotesSet(filter_input(INPUT_POST, 'upvotes', FILTER_SANITIZE_NUMBER_INT));
        else if( array_key_exists('shares', $_POST) )
            $user->setIsEmailNotificationSharesSet(filter_input(INPUT_POST, 'shares', FILTER_SANITIZE_NUMBER_INT));
        else if( array_key_exists('subposts', $_POST) )
            $user->setIsEmailNotificationSubpostsSet(filter_input(INPUT_POST, 'subposts', FILTER_SANITIZE_NUMBER_INT));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }
        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }

    public function getManyByPartialUsernameAction($username)
    {
        $userObjects = $this->_getUserByUsernameSearch($username . '%');

        $userStdClasses = array();
        foreach( $userObjects AS $userObject ){
            $userStdClass = $userObject->toStdClass();
            $userStdClass->url = $this->generateUrl('UserPage', array('userid' => $userObject->getId(), 'username' => $userObject->getUsername()));
            $userStdClasses[] = $userStdClass;
        }

        return new Response(json_encode($userStdClasses), 200, array('Content-Type'=>'application/json'));
    }

    public function postHideToDoListAction()
    {
        $this->getUser()->setShowToDoList(FALSE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($this->getUser());
        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('success' => 0, 'error' => $e->getPrevious()->getCode())), 200, array('Content-Type'=>'application/json'));
        }
        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }

    public function postTodoPageDoneAction($pageNumber)
    {
        $setTodoPageFunction = 'setIsTodoPage' . $pageNumber . 'Done';
        $this->getUser()->$setTodoPageFunction(TRUE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($this->getUser());
        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('success' => 0, 'error' => $e->getPrevious()->getCode())), 200, array('Content-Type'=>'application/json'));
        }
        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }
}
