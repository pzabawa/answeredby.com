<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="conversations")
 */
class Conversation extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="users_conversations")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="conversations_with_user")
     * @ORM\JoinColumn(name="destination_user_id", referencedColumnName="id", nullable=false)
     */
    protected $destination_user;

    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="conversation", cascade={"persist","remove"})
     * @ORM\OrderBy({"createdDatetime" = "DESC"})
     */
    protected $messages;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Conversation
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->messages = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($createdUser);
    }

    /**
     * Set destination_user
     *
     * @param \Answeredby\EntityBundle\Entity\User $destinationUser
     * @return Conversation
     */
    public function setDestinationUser(\Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        $this->destination_user = $destinationUser;

        return $this;
    }

    /**
     * Get destination_user
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getDestinationUser()
    {
        return $this->destination_user;
    }

    /**
     * Add messages
     *
     * @param \Answeredby\EntityBundle\Entity\Message $messages
     * @return Conversation
     */
    public function addMessage(\Answeredby\EntityBundle\Entity\Message $messages)
    {
        $this->messages[] = $messages;

        return $this;
    }

    /**
     * Remove messages
     *
     * @param \Answeredby\EntityBundle\Entity\Message $messages
     */
    public function removeMessage(\Answeredby\EntityBundle\Entity\Message $messages)
    {
        $this->messages->removeElement($messages);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Get the unopened messages from the user who isn't $userNotToCountMessagesBy
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUnopenedMessagesNotByUser(\Answeredby\EntityBundle\Entity\User $userNotToCountMessagesBy)
    {
        return $this->getMessages()->matching(Criteria::create()
                ->where(Criteria::expr()->neq("createdUser", $userNotToCountMessagesBy))
                ->andWhere(Criteria::expr()->eq("has_destination_user_opened", FALSE)));
    }

    /**
     * Get createdUser or destination_user, whichever isn't $userNotToReturn
     *
     * @return \Answeredby\EntityBundle\Entity\User
     */
    public function getUserWhoIsNot(\Answeredby\EntityBundle\Entity\User $userNotToReturn)
    {
        if( $this->getCreatedUser() == $userNotToReturn )
            return $this->getDestinationUser();
        return $this->getCreatedUser();
    }
}
