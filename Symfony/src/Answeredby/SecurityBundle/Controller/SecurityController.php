<?php

namespace Answeredby\SecurityBundle\Controller;

use Answeredby\SecurityBundle\Controller\SecurityBundleController;

class SecurityController extends SecurityBundleController
{
    public function loginAction()
    {
        return $this->_loginAction('AnsweredbySecurityBundle:Security:login.html.twig');
    }

    public function registerAction()
    {
        return $this->_loginAction('AnsweredbySecurityBundle:Security:register.html.twig');
    }
}