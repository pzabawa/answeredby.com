<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="postShares")
 */
class PostShare extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="postShares")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false)
     */
    protected $post;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="postShares")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\OneToMany(targetEntity="PostShareDestinationUser", mappedBy="postShare", cascade={"persist","remove"})
     */
    protected $postShareDestinationUsers;

    /**
     * @ORM\ManyToMany(targetEntity="Network")
     * @ORM\JoinTable(name="postShareNetworks")
     **/
    protected $networks;

    /**
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="new_postshare", cascade={"persist"})
     **/
    protected $notifications;

    /**
     * @ORM\Column(name="message", type="string", length=255, nullable=true)
     */
    protected $message;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->postShareDestinationUsers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->networks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notifications = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($createdUser);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return PostShare
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set post
     *
     * @param \Answeredby\EntityBundle\Entity\Post $post
     * @return PostShare
     */
    public function setPost(\Answeredby\EntityBundle\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \Answeredby\EntityBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return PostShare
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Add postShareDestinationUsers
     *
     * @param \Answeredby\EntityBundle\Entity\PostShareDestinationUser $postShareDestinationUser
     * @return PostShare
     */
    public function addPostShareDestinationUser(\Answeredby\EntityBundle\Entity\PostShareDestinationUser $postShareDestinationUser)
    {
        $this->postShareDestinationUsers[] = $postShareDestinationUser;

        return $this;
    }

    /**
     * Remove postShareDestinationUsers
     *
     * @param \Answeredby\EntityBundle\Entity\PostShareDestinationUser $postShareDestinationUser
     */
    public function removePostShareDestinationUser(\Answeredby\EntityBundle\Entity\PostShareDestinationUser $postShareDestinationUser)
    {
        $this->postShareDestinationUsers->removeElement($postShareDestinationUser);
    }

    /**
     * Get postShareDestinationUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPostShareDestinationUsers()
    {
        return $this->postShareDestinationUsers;
    }

    /**
     * Get postShareDestinationUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPostShareDestinationUsersWithDestinationUser(\Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        return $this->getPostShareDestinationUsers()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("destinationUser", $destinationUser)));
    }

    /**
     * Add networks
     *
     * @param \Answeredby\EntityBundle\Entity\Network $networks
     * @return PostShare
     */
    public function addNetwork(\Answeredby\EntityBundle\Entity\Network $networks)
    {
        $this->networks[] = $networks;

        return $this;
    }

    /**
     * Remove networks
     *
     * @param \Answeredby\EntityBundle\Entity\Network $networks
     */
    public function removeNetwork(\Answeredby\EntityBundle\Entity\Network $networks)
    {
        $this->networks->removeElement($networks);
    }

    /**
     * Get networks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNetworks()
    {
        return $this->networks;
    }

    /**
     * Add notifications
     *
     * @param \Answeredby\EntityBundle\Entity\Notification $notifications
     * @return PostShare
     */
    public function addNotification(\Answeredby\EntityBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \Answeredby\EntityBundle\Entity\Notification $notifications
     */
    public function removeNotification(\Answeredby\EntityBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
}
