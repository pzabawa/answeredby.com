<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\Relationship;

class RelationshipController extends Controller
{
    protected function _isUserAuthorizedToAccessRelationship($postOriginationUserId, $postDestinationUserId)
    {
        if( $postOriginationUserId != $this->getUser()->getId()
         && $postDestinationUserId != $this->getUser()->getId() )
            return FALSE;
        return TRUE;
    }

    protected function _getSetApprovedByCurrentUserFunction($postOriginationUserId)
    {
        return $postOriginationUserId == $this->getUser()->getId() ? 'setApprovedByOriginationUser' : 'setApprovedByDestinationUser';
    }

    public function postAction()
    {
        // Exit if no POST data.
        if( empty($_POST) )
            return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));

        // Get POST data.
        $postOriginationUserId   = filter_input(INPUT_POST, 'origination_user_id', FILTER_SANITIZE_NUMBER_INT);
        $postDestinationUserId   = filter_input(INPUT_POST, 'destination_user_id', FILTER_SANITIZE_NUMBER_INT);
        $postNetworkIds = filter_input(INPUT_POST, 'network_ids', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

        // Exit if unauthorized to access this Relationship.
        if( !$this->_isUserAuthorizedToAccessRelationship($postOriginationUserId, $postDestinationUserId))
            return new Response(json_encode(array('error' => 'Unauthorized.')), 401, array('Content-Type'=>'application/json'));

        $entityManager = $this->getDoctrine()->getManager();
        $postNetworkIdsAssociativeArray = $postNetworkIds ? array_flip($postNetworkIds) : array();
        $approvedByCurrentUserFieldName   = $postOriginationUserId == $this->getUser()->getId() ? 'approvedByOriginationUser'    : 'approvedByDestinationUser';
        $getApprovedByOtherUserFunction   = $postOriginationUserId == $this->getUser()->getId() ? 'getApprovedByDestinationUser' : 'getApprovedByOriginationUser';
        $setApprovedByCurrentUserFunction = $this->_getSetApprovedByCurrentUserFunction($postOriginationUserId);

        // For each existing Relationship that's been approved by the current user,
        //  check if it exists in the POST data.
        //   If so, remove the Relationship from the POST data to leave a list of Relationships not in the database.
        //   If not, remove the "approved by current user" flag, and if not approved by the other user, remove the Relationship from the database.
        //
        // Then create Relationships for those in the POST that weren't in the database -- the remaining Relationships in the POST data.
        // (They may already exist if they've been approved by the other User.)

        // For each existing Relationship that's been approved by the current user...
        $existingRelationshipsApprovedByCurrentUser = $entityManager->getRepository('AnsweredbyEntityBundle:Relationship')->findBy(array('originationUser' => $postOriginationUserId, 'destination_user' => $postDestinationUserId, $approvedByCurrentUserFieldName => TRUE));
        foreach( $existingRelationshipsApprovedByCurrentUser AS $existingRelationshipApprovedByCurrentUser ){
            // Check if it exists in the POST data...
            if( array_key_exists($existingRelationshipApprovedByCurrentUser->getNetwork()->getId(), $postNetworkIdsAssociativeArray) ){
                // If so, remove the Relationship from the POST data to leave a list of Relationships not in the database...
                unset($postNetworkIdsAssociativeArray[$existingRelationshipApprovedByCurrentUser->getNetwork()->getId()]);
            // If not, remove the "approved by current user" flag, and if not approved by the other user, remove the Relationship from the database.
            } else if( $existingRelationshipApprovedByCurrentUser->$getApprovedByOtherUserFunction() ){
                $existingRelationshipApprovedByCurrentUser->$setApprovedByCurrentUserFunction(FALSE);
                // If the non-created User is unapproving this, they don't want to be notified of it.
                // If the created User is unapproving this, the non-created User has already approved it (else this if condition wouldn't have been met) and could only set this flag again or delete this Relationships, so this doesn't matter.
                $existingRelationshipApprovedByCurrentUser->setIgnoredByNonCreatedUser(TRUE);
            } else {
                $entityManager->remove($existingRelationshipApprovedByCurrentUser);
            }
        }

        $originationUserReference = $entityManager->getReference('AnsweredbyEntityBundle:User', $postOriginationUserId);
        $destinationUserReference = $entityManager->getReference('AnsweredbyEntityBundle:User', $postDestinationUserId);

        // Then create Relationships for those in the POST that weren't in the database -- the remaining Relationships in the POST data.
        // (They may already exist if they've been approved by the other User.)
        foreach( $postNetworkIdsAssociativeArray AS $postNetworkId => $arrayIterator ){
            $relationshipToApprove = $entityManager->find('Answeredby\EntityBundle\Entity\Relationship', array('originationUser' => $postOriginationUserId, 'destination_user' => $postDestinationUserId, 'network' => $postNetworkId));
            if( !$relationshipToApprove ){
                $relationshipToApprove = new Relationship($this->getUser(),
                                                          $originationUserReference,
                                                          $destinationUserReference,
                                                          $entityManager->find('Answeredby\EntityBundle\Entity\Network', $postNetworkId));
            }

            $relationshipToApprove->$setApprovedByCurrentUserFunction(TRUE);

            $entityManager->persist($relationshipToApprove);
        }

        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }

    public function postApproveOrIgnoreAction()
    {
        // Exit if no POST data.
        if( empty($_POST) )
            return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));

        // Get POST data.
        $postOriginationUserId  = filter_input(INPUT_POST, 'origination_user_id', FILTER_SANITIZE_NUMBER_INT);
        $postDestinationUserId  = filter_input(INPUT_POST, 'destination_user_id', FILTER_SANITIZE_NUMBER_INT);
        $postNetworkId = filter_input(INPUT_POST, 'network_id', FILTER_SANITIZE_NUMBER_INT);

        // Exit if unauthorized to access this Relationship.
        if( !$this->_isUserAuthorizedToAccessRelationship($postOriginationUserId, $postDestinationUserId))
            return new Response(json_encode(array('error' => 'Unauthorized.')), 401, array('Content-Type'=>'application/json'));

        $entityManager = $this->getDoctrine()->getManager();
        $setApprovedByCurrentUserFunction = $this->_getSetApprovedByCurrentUserFunction($postOriginationUserId);

        $relationshipToApproveOrIgnore = $entityManager->find('Answeredby\EntityBundle\Entity\Relationship', array('originationUser' => $postOriginationUserId, 'destination_user' => $postDestinationUserId, 'network' => $postNetworkId));
        if( !$relationshipToApproveOrIgnore ){
            $relationshipToApproveOrIgnore = new Relationship($this->getUser(),
                                                              $entityManager->getReference('AnsweredbyEntityBundle:User', $postOriginationUserId),
                                                              $entityManager->getReference('AnsweredbyEntityBundle:User', $postDestinationUserId),
                                                              $entityManager->getReference('AnsweredbyEntityBundle:Network', $postNetworkId));
        }

        // Set the Relationship's value given by the POST.
        if( array_key_exists('ignore_relationship_boolean', $_POST)
         && filter_input(INPUT_POST, 'ignore_relationship_boolean', FILTER_SANITIZE_NUMBER_INT) ){
            $relationshipToApproveOrIgnore->setIgnoredByNonCreatedUser(TRUE);
            $relationshipToApproveOrIgnore->$setApprovedByCurrentUserFunction(FALSE);
        } else {
            if( array_key_exists('unselect', $_POST)
             && filter_input(INPUT_POST, 'unselect', FILTER_SANITIZE_NUMBER_INT) ){
                $relationshipToApproveOrIgnore->$setApprovedByCurrentUserFunction(FALSE);
                $relationshipToApproveOrIgnore->setIgnoredByNonCreatedUser(TRUE);
            } else {
                $relationshipToApproveOrIgnore->$setApprovedByCurrentUserFunction(TRUE);
            }
        }

        // If the Relatioship isn't approved by either party any more, delete it.
        if( !$relationshipToApproveOrIgnore->getApprovedByDestinationUser() 
         && !$relationshipToApproveOrIgnore->getApprovedByOriginationUser() )
            $entityManager->remove($relationshipToApproveOrIgnore);
        else
            $entityManager->persist($relationshipToApproveOrIgnore);

        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }

    public function postApproveOrUnapproveAction()
    {
        // Exit if no POST data.
        if( empty($_POST) )
            return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));

        // Get POST data.
        $postOriginationUserId  = filter_input(INPUT_POST, 'origination_user_id', FILTER_SANITIZE_NUMBER_INT);
        $postDestinationUserId  = filter_input(INPUT_POST, 'destination_user_id', FILTER_SANITIZE_NUMBER_INT);
        $postNetworkId = filter_input(INPUT_POST, 'network_id', FILTER_SANITIZE_NUMBER_INT);

        // Exit if unauthorized to access this Relationship.
        if( $postDestinationUserId != $this->getUser()->getId() )
            return new Response(json_encode(array('error' => 'Unauthorized.')), 401, array('Content-Type'=>'application/json'));

        $entityManager = $this->getDoctrine()->getManager();
        $relationshipToApproveOrUnapprove = $entityManager->find('Answeredby\EntityBundle\Entity\Relationship', array('originationUser' => $postOriginationUserId, 'destination_user' => $postDestinationUserId, 'network' => $postNetworkId));
        if( !$relationshipToApproveOrUnapprove )
            $relationshipToApproveOrUnapprove = new Relationship($this->getUser(),
                                                                 $entityManager->getReference('AnsweredbyEntityBundle:User', $postOriginationUserId),
                                                                 $entityManager->getReference('AnsweredbyEntityBundle:User', $postDestinationUserId),
                                                                 $entityManager->getReference('AnsweredbyEntityBundle:Network', $postNetworkId));

        $relationshipToApproveOrUnapprove->setApprovedByDestinationUser(filter_input(INPUT_POST, 'approve_network', FILTER_SANITIZE_NUMBER_INT));

        // If the Relatioship isn't approved by either party any more, delete it.
        if( !$relationshipToApproveOrUnapprove->getApprovedByDestinationUser() 
         && !$relationshipToApproveOrUnapprove->getApprovedByOriginationUser() )
            $entityManager->remove($relationshipToApproveOrUnapprove);
        else
            $entityManager->persist($relationshipToApproveOrUnapprove);

        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }
}
