<?php

namespace Answeredby\SecurityBundle\Handler;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Doctrine\ORM\EntityManager;

class AuthenticationHandler implements AuthenticationSuccessHandlerInterface, AuthenticationFailureHandlerInterface
{
    private $router;
    private $session;

    /**
     * Constructor
     *
     * @author     Joe Sexton <joe@webtipblog.com>
     * @param     RouterInterface $router
     * @param     Session $session
     */
    public function __construct(RouterInterface $router, Session $session, EntityManager $em, \Answeredby\SharedBundle\Helper\TwigGlobalsHelper $twigGlobalsHelper)
    {
        $this->router = $router;
        $this->session = $session;
        $this->em = $em;
        $this->twigGlobalsHelper = $twigGlobalsHelper;
    }

    /**
     * onAuthenticationSuccess
      *
     * @author     Joe Sexton <joe@webtipblog.com>
     * @param     Request $request
     * @param     TokenInterface $token
     * @return     Response
     */
    public function onAuthenticationSuccess( Request $request, TokenInterface $token )
    {
        // if AJAX login
        if ( $request->isXmlHttpRequest() ) {
            $data = array();
            $data['id'] = $token->getUser()->getId();
            $data['username'] = $token->getUser()->getUsername();
            $data['userProfilePageUrl'] = $this->router->generate( 'UserPage', array('userid' => $token->getUser()->getId(), 'username' => $token->getUser()->getUrlUsername()) );
            $data['userProfilePicUrl'] = $token->getUser()->getProfilePicUrl();
            $data['userBackgroundHtmlColor'] = $token->getUser()->getBackgroundHtmlColor();
            $data['userNetworks'] = array();
            foreach( $token->getUser()->getNetworks() AS $network ){
                $data['userNetworks'][$network->getId()] = $network->getText(); 
            }
            $data['unopenedMessagesCount'] = $token->getUser()->getUnopenedMessagesCount();
            $data['notifications'] = array();
            foreach( $this->twigGlobalsHelper->getUsersNotifications(10) AS $notification ){
                $notificationStdClass = new \stdClass();
                $notificationStdClass->createdUserImageUrl = $notification->getCreatedUser()->getProfilePicUrl();
                $notificationStdClass->createdUserBackgroundHtmlColor = $notification->getCreatedUser()->getBackgroundHtmlColor();
                $notificationStdClass->notificationDescription = $notification->getDescription();
                $notificationStdClass->notificationOpened = $notification->getOpened();
                $notificationStdClass->postUrl = $this->router->generate('PostPage', array('post_id' => $notification->getTargetPost()->getId()));
                $notificationStdClass->createdDatetime = $notification->getCreatedDatetime()->format('Y-m-d\\TH:i:sP');
                $data['notifications'][] = $notificationStdClass;
            }
            $data['unviewedNotificationsCount'] = count($this->twigGlobalsHelper->getUsersUnviewedNotifications());

            $arrayOfPostIds = filter_input(INPUT_POST, 'arrayOfPostIds', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
            $data['userPostPostIds'] = array();
            if( $arrayOfPostIds ){
                foreach( $arrayOfPostIds AS $postId ){
                    $post = $this->em->getReference('AnsweredbyEntityBundle:Post', $postId);
                    if( !$token->getUser()->getUserPostsWithPost($post)->isEmpty() ){
                        $data['userPostPostIds'][] = $postId;
                    }
                }
            }

            $array = array( 'success' => true, 'data' => $data ); // data to return via JSON
            $response = new Response( json_encode( $array ) );
            $response->headers->set( 'Content-Type', 'application/json' );

            return $response;

        // if form login 
        } else {

            $target_url = $this->router->generate( 'HomePage' );
            $target_fragment = "";
            $session = $request->getSession();

            if( $request->query->get('target_url') ){
                $target_url = filter_var($request->query->get('target_url'), FILTER_SANITIZE_STRING);
                $session->set('target_url', $target_url);
            }
            if( $request->query->get('target_fragment') ){
                $target_fragment = filter_var($request->query->get('target_fragment'), FILTER_SANITIZE_STRING);
                $session->set('target_fragment', $target_fragment);
            }
            if( $session->has('target_url') ){
                $target_url = $session->get('target_url');
            }
            if( $session->has('target_fragment') ){
                $target_fragment = $session->get('target_fragment');
            }

            // Remove the session targets used only for this page.
            $session->remove('target_url');
            $session->remove('target_fragment');

            return new RedirectResponse($target_url . '#' . $target_fragment);
        }
    }
 
    /**
     * onAuthenticationFailure
     *
     * @author     Joe Sexton <joe@webtipblog.com>
     * @param     Request $request
     * @param     AuthenticationException $exception
     * @return     Response
     */
     public function onAuthenticationFailure( Request $request, AuthenticationException $exception )
    {
        // if AJAX login
        if ( $request->isXmlHttpRequest() ) {

            $array = array( 'success' => false, 'message' => $exception->getMessage() ); // data to return via JSON
            $response = new Response( json_encode( $array ) );
            $response->headers->set( 'Content-Type', 'application/json' );

            return $response;

        // if form login 
        } else {

            $session = $request->getSession();

            if( $request->query->get('target_url') ){
                $target_url = filter_var($request->query->get('target_url'), FILTER_SANITIZE_STRING);
                $session->set('target_url', $target_url);
            }
            if( $request->query->get('target_fragment') ){
                $target_fragment = filter_var($request->query->get('target_fragment'), FILTER_SANITIZE_STRING);
                $session->set('target_fragment', $target_fragment);
            }

            // set authentication exception to session
            $request->getSession()->set(SecurityContextInterface::AUTHENTICATION_ERROR, $exception);

            return new RedirectResponse( $this->router->generate( 'login' ) );
        }
    }
}