#!/bin/bash

# No prompts.
export DEBIAN_FRONTEND=noninteractive

# Set up mysql password.
sudo debconf-set-selections <<< 'mysql-server-5.6 mysql-server/root_password password gEEb?gEEb!'
sudo debconf-set-selections <<< 'mysql-server-5.6 mysql-server/root_password_again password gEEb?gEEb!'

# Add PHP5.5 repo.
apt-get update
apt-get -y install python-software-properties
add-apt-repository ppa:ondrej/php5

# Update and install packages.
apt-get update
# apt-get -y dist-upgrade
apt-get -y install vim php5-curl curl libcurl3 libcurl3-dev nginx php5-cli php5-fpm php5-mysql mysql-client mysql-server php5-intl git
apt-get -y autoremove

# php
sed -i 's/;date.timezone =/date.timezone = America\/Detroit/g' /etc/php5/cli/php.ini
sed -i 's/;date.timezone =/date.timezone = America\/Detroit/g' /etc/php5/fpm/php.ini
service php5-fpm restart

# Symfony
mkdir /var/Symfony
mkdir /var/Symfony/cache
chmod a+w /var/Symfony/cache
mkdir /var/Symfony/logs
chmod a+w /var/Symfony/logs
mkdir /var/Symfony/sessions
chmod a+w /var/Symfony/sessions

# nginx
rm /etc/nginx/sites-enabled/default
cp /vagrant/nginx.answeredby /etc/nginx/sites-available/answeredby
ln -s /etc/nginx/sites-available/answeredby /etc/nginx/sites-enabled/answeredby
ln -s /vagrant/Symfony/web /srv/answeredby
service nginx restart

# mysql
echo "create database answeredby" | mysql -u root -pgEEb?gEEb!

# clear cache and reset database and crons
sh /vagrant/afterGitPull.sh

