<?php

namespace Answeredby\ViewBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;

class HomeController extends SharedBundleController
{
    public function indexAction()
    {
        if( $this->getUser() ){

            // Get if this the User's first time to the Homepage. Then remove that flag, since every time after this won't be.
            $session = $this->getRequest()->getSession();
            $firstTimeToHomepage = $session->has('first_time_to_homepage') ? $session->get('first_time_to_homepage') : FALSE;
            if( $session->has('first_time_to_homepage') )
                $session->remove('first_time_to_homepage');

            return $this->render('AnsweredbyViewBundle:Home:loggedin.html.twig', array('firstTimeToHomepage' => $firstTimeToHomepage));
        }
        return $this->_loginAction('AnsweredbyViewBundle:Home:index.html.twig');
    }
}
