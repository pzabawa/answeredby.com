<?php

namespace Answeredby\SecurityBundle\Controller;

use Answeredby\SecurityBundle\Controller\SecurityBundleController;

class RelationshipController extends SecurityBundleController
{
    public function indexAction()
    {
        if( !$this->_isAuthenticated() )
            return $this->redirect($this->generateUrl('login') . "?target_url=" . $this->generateUrl('RelationshipsPage', array(), TRUE));

        return $this->render('AnsweredbySecurityBundle:Relationship:index.html.twig');
    }
}
