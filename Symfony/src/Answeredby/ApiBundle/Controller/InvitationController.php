<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\Invitation;
use Answeredby\EntityBundle\Entity\UserInvitation;
use Answeredby\EntityBundle\Entity\Notification;
use Answeredby\EntityBundle\Entity\Relationship;

class InvitationController extends Controller
{
    public function postAction()
    {
        if( !empty($_POST) ){
            $entityManager = $this->getDoctrine()->getManager();

            // Check if valid e-mail address.
            if( !filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL) ){
                return new Response(json_encode(array('error' => 'Invalid e-mail format.')), 400, array('Content-Type'=>'application/json'));
            }
            $emailAddress = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $networkIds =  filter_input(INPUT_POST, 'networks', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

            $userWithEmail = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->findOneByEmail($emailAddress);
            if( $userWithEmail ){
                $entityManager->persist(new Notification($this->getUser(), $userWithEmail));

                if( $networkIds )
                    foreach( $networkIds AS $networkId )
                        $entityManager->merge(new Relationship($this->getUser(),
                                                                 $this->getUser(),
                                                                 $userWithEmail,
                                                                 $entityManager->getReference('AnsweredbyEntityBundle:Network', $networkId)));
            } else {
                $invitation = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Invitation')->findOneByEmail($emailAddress);
                if( !$invitation ){
                    $invitation = new Invitation($this->getUser());
                    $invitation->setEmail($emailAddress);

                    $entityManager->persist($invitation);
                    try {
                        $entityManager->flush();
                    } catch (\Doctrine\DBAL\DBALException $e) {
                        return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
                    }
                }

                $userInvitation = $entityManager->find('Answeredby\EntityBundle\Entity\UserInvitation', array('createdUser' => $this->getUser()->getId(), 'invitation' => $invitation->getId()));
                if( !$userInvitation ){
                    $userInvitation = new UserInvitation($this->getUser(), $invitation);
                } else {
                    $userInvitation->setWasEmailSent(FALSE);
                }

                if( $networkIds )
                    foreach( $networkIds AS $networkId )
                        $userInvitation->addNetwork($entityManager->getReference('AnsweredbyEntityBundle:Network', $networkId));

                $entityManager->persist($userInvitation);
            }

            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }

    public function unsubscribeAction()
    {
        if( empty($_POST) )
            return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));

        $invitation = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Invitation')->findOneByMd5(filter_input(INPUT_POST, 'md5', FILTER_SANITIZE_STRING));
        if( !$invitation )
            return new Response(json_encode(array('error' => 'No invitation found.')), 400, array('Content-Type'=>'application/json'));

        $invitation->setIsUnsubscribed(filter_input(INPUT_POST, 'unsubscribe', FILTER_SANITIZE_NUMBER_INT));

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($invitation);
        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }
}
