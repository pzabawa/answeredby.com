<?php

namespace Answeredby\ViewBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;

class PostController extends SharedBundleController
{
    public function indexAction($post_id)
    {
        $postObject = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Post')->find($post_id);

        if( !is_a($postObject, 'Answeredby\EntityBundle\Entity\Post') ){
            throw $this->createNotFoundException('No post found.');
        }

        // Set Notification as opened.
        if( array_key_exists('notification_id', $_GET) ){
            $notification = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Notification')->findOneByMd5(filter_input(INPUT_GET, 'notification_id', FILTER_SANITIZE_STRING));
            if( $notification && $notification->getDestinationUser() == $this->getUser() && !$notification->getOpened() ){
                $notification->setOpened(TRUE);

                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($notification);
                try {
                    $entityManager->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {}
            }
        }

        if( $this->_isPostAccessibleByCurrentUser($postObject) ){
            if( $this->getUser() )
                return $this->render('AnsweredbyViewBundle:Post:index.html.twig', array('post' => $postObject));
            return $this->render('AnsweredbyViewBundle:Post:index.html.twig', array('post' => $postObject));
        }
        return $this->render('AnsweredbyViewBundle:Post:index.html.twig');
    }
}
