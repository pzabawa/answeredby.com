<?php

namespace Answeredby\ViewBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;

class InvitationController extends SharedBundleController
{
    public function indexAction($md5)
    {
        $invitation = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Invitation')->findOneByMd5($md5);
        if( !$invitation )
            throw $this->createNotFoundException("No invitation found.");

        // Remove the session targets if responding to an invitation.
        $this->getRequest()->getSession()->remove('target_url');
        $this->getRequest()->getSession()->remove('target_fragment');

        return $this->_loginAction('AnsweredbyViewBundle:Home:index.html.twig', array('invitation' => $invitation, 'doNotShowSidebar' => TRUE));
    }
}
