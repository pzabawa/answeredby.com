<?php

namespace Answeredby\SecurityBundle\Controller;

use Answeredby\SecurityBundle\Controller\SecurityBundleController;

class AccountController extends SecurityBundleController
{   
    public function indexAction()
    {
        if( !$this->_isAuthenticated() )
            return $this->redirect($this->generateUrl('login') . "?target_url=" . $this->generateUrl('AccountPage', array(), TRUE));

        if( array_key_exists('delete', $_GET)
         && array_key_exists('confirm_code', $_GET) ){
            if( filter_input(INPUT_GET, 'delete', FILTER_SANITIZE_NUMBER_INT) == 1
             && filter_input(INPUT_GET, 'confirm_code', FILTER_SANITIZE_STRING) == $this->getUser()->getMd5() ){
                $entityManager = $this->getDoctrine()->getManager();

                // Detach all the Invitations attached to this User.
                if( $this->getUser()->getInvitation() )
                    $this->getUser()->getInvitation()->setDestinationUser();
                foreach( $this->getUser()->getInvitations() AS $invitation )
                    $invitation->setCreatedUser();

                // Delete the User.
                $entityManager->remove($this->getUser());
                $entityManager->flush();

                $this->get('security.context')->setToken(null);
                $this->get('request')->getSession()->invalidate();
                return $this->redirect($this->get('router')->generate('HomePage') . '?delete=1', 307);
            }
        }

        return $this->render('AnsweredbySecurityBundle:Account:index.html.twig');
    }
}
