<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\Network;

class NetworkController extends Controller
{
    public function postAction()
    {
        if( !empty($_POST) ){
            $entityManager = $this->getDoctrine()->getManager();

            $network = new Network($this->getUser());
            $network->updateFromPostArray($_POST);

            $entityManager->persist($network);
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            return new Response(json_encode(array('id' => $network->getId())), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }

    public function deleteAction($network_id)
    {
        $network = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Network')->find($network_id);

        if( !$network )
            return new Response(json_encode(array('error' => 'Network not found')), 404, array('Content-Type'=>'application/json'));

        if( $network->getCreatedUser() != $this->getUser() )
            return new Response(json_encode(array('error' => 'Unauthorized')), 401, array('Content-Type'=>'application/json'));

        if( $network->getIsIndividualSharesType() 
         || $network->getIsDefaultType() )
            return new Response(json_encode(array('error' => 'Unable to delete permanent network')), 401, array('Content-Type'=>'application/json'));

        $network->setIsHidden(TRUE);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($network);
        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }
}
