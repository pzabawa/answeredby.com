<?php

namespace Answeredby\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class InvitationsUnsubscribeController extends Controller
{
    public function indexAction($md5)
    {
        $invitation = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Invitation')->findOneByMd5($md5);
        if( !$invitation )
            throw $this->createNotFoundException("No invitation found.");
        return $this->render('AnsweredbyViewBundle:InvitationsUnsubscribe:index.html.twig', array('md5' => $md5, 'invitation' => $invitation));
    }
}
