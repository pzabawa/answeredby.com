<?php

namespace Answeredby\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ViewBundleController extends Controller
{
    protected function _integerNotificationFrequencyToDescription($integerFrequency)
    {
        switch ($integerFrequency) {
            case 3: return "Instantly";
            case 2: return "Daily";
            case 1: return "Weekly";
        }
        return "Never";
    }
}
