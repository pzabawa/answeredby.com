<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="userInvitations")
 */
class UserInvitation extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userInvitations")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Invitation", inversedBy="userInvitations")
     * @ORM\JoinColumn(name="invitation_id", referencedColumnName="id", nullable=false)
     */
    protected $invitation;

    /**
     * @ORM\Column(name="isHiddenByInvitationDestinationUser", type="boolean")
     */
    protected $isHiddenByInvitationDestinationUser;

    /**
     * @ORM\Column(name="wasEmailSent", type="boolean")
     */
    protected $wasEmailSent;

    /**
     * @ORM\ManyToMany(targetEntity="Network")
     * @ORM\JoinTable(name="userInvitationNetworks",
     *                joinColumns={@ORM\JoinColumn(name="user_invitation_created_user_id", referencedColumnName="created_user_id",onDelete="CASCADE"),@ORM\JoinColumn(name="user_invitation_invitation_id", referencedColumnName="invitation_id",onDelete="CASCADE")},
     *                inverseJoinColumns={@ORM\JoinColumn(name="network_id", referencedColumnName="id",onDelete="CASCADE")}
     *                )
     **/
    protected $networks;

    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser,
                                \Answeredby\EntityBundle\Entity\Invitation $invitation)
    {
        $this->_setInvitation($invitation);
        $this->setIsHiddenByInvitationDestinationUser(FALSE);
        $this->setWasEmailSent(FALSE);
        $this->networks = new \Doctrine\Common\Collections\ArrayCollection();

        parent::__construct($createdUser);
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return UserInvitation
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set invitation
     *
     * @param \Answeredby\EntityBundle\Entity\Invitation $invitation
     * @return UserInvitation
     */
    protected function _setInvitation(\Answeredby\EntityBundle\Entity\Invitation $invitation)
    {
        $this->invitation = $invitation;

        return $this;
    }

    /**
     * Get invitation
     *
     * @return \Answeredby\EntityBundle\Entity\Invitation 
     */
    public function getInvitation()
    {
        return $this->invitation;
    }

    /**
     * Set isHiddenByInvitationDestinationUser
     *
     * @param boolean $isHiddenByInvitationDestinationUser
     * @return UserInvitation
     */
    public function setIsHiddenByInvitationDestinationUser($isHiddenByInvitationDestinationUser)
    {
        $this->isHiddenByInvitationDestinationUser = $isHiddenByInvitationDestinationUser;

        return $this;
    }

    /**
     * Get isHiddenByInvitationDestinationUser
     *
     * @return boolean 
     */
    public function getIsHiddenByInvitationDestinationUser()
    {
        return $this->isHiddenByInvitationDestinationUser;
    }

    /**
     * Set wasEmailSent
     *
     * @param boolean $wasEmailSent
     * @return UserInvitation
     */
    public function setWasEmailSent($wasEmailSent)
    {
        $this->wasEmailSent = $wasEmailSent;

        return $this;
    }

    /**
     * Get wasEmailSent
     *
     * @return boolean 
     */
    public function getWasEmailSent()
    {
        return $this->wasEmailSent;
    }

    /**
     * Add networks
     *
     * @param \Answeredby\EntityBundle\Entity\Network $networks
     * @return UserInvitation
     */
    public function addNetwork(\Answeredby\EntityBundle\Entity\Network $networks)
    {
        $this->networks[] = $networks;

        return $this;
    }

    /**
     * Remove networks
     *
     * @param \Answeredby\EntityBundle\Entity\Network $networks
     */
    public function removeNetwork(\Answeredby\EntityBundle\Entity\Network $networks)
    {
        $this->networks->removeElement($networks);
    }

    /**
     * Get networks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNetworks()
    {
        return $this->networks;
    }
}
