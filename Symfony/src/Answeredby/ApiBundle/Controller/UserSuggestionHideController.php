<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\UserSuggestionHide;

class UserSuggestionHideController extends Controller
{
    public function postAction()
    {
        if( !empty($_POST) ){
            $entityManager = $this->getDoctrine()->getManager();
            $userSuggestionHide = new UserSuggestionHide($this->getUser(),
                                     $entityManager->getReference('AnsweredbyEntityBundle:User', filter_input(INPUT_POST, 'destination_user_id', FILTER_SANITIZE_NUMBER_INT)));
            $entityManager->detach($userSuggestionHide);
            $entityManager->merge($userSuggestionHide);
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }
}
