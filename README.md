# Codebase: answeredby.com #

* This repository is the codebase for a *defunct* website, https://answeredby.com.
* It's currently hosted, though not completely functional, at http://answeredby.patzabawa.site.
* The goal of this website was to be a social network similar to Facebook, but one in which people could post to one or more posting groups of theirs, and all recipients had to approve their membership of this group. 
* This site was build on Symfony 2.3, http://symfony.com/doc/2.3/index.html.


### How do I get set up? ###

* Navigate to the project folder.
* Run "vagrant up". (Vagrant must be installed from https://www.vagrantup.com/.)
* Navigate to http://localhost:8080/app_dev.php/.

### Who do I talk to? ###

* This repository was solely built and maintained by Pat Zabawa, patzabawa@gmail.com.