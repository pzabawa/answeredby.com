<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="cronjobs")
 */
class CronJob
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="text")
     */
    protected $name;

    /**
     * @ORM\Column(name="last_run_datetime", type="datetime")
     */
    protected $last_run_datetime;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CronJob
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set last_run_datetime
     *
     * @param \DateTime $lastRunDatetime
     * @return CronJob
     */
    public function setLastRunDatetime($lastRunDatetime)
    {
        $this->last_run_datetime = $lastRunDatetime;

        return $this;
    }

    /**
     * Get last_run_datetime
     *
     * @return \DateTime 
     */
    public function getLastRunDatetime()
    {
        return $this->last_run_datetime;
    }
}
