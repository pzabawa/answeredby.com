<?php

namespace Answeredby\SecurityBundle\Controller;

use Answeredby\SharedBundle\Controller\SharedBundleController;

class SecurityBundleController extends SharedBundleController
{
    protected function _isAuthenticated()
    {
        $securityContext = $this->container->get('security.context');
        return $securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED');
    }
}
