<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="userPostHides")
 */
class UserPostHide extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="userPostHides")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false)
     */
    protected $post;

    /**
     * @ORM\Column(name="is_marked_spam", type="boolean")
     */
    protected $is_marked_spam;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser,
                                \Answeredby\EntityBundle\Entity\Post $post,
                                $isMarkedSpam)
    {
        $this->_setPost($post);
        $this->_setIsMarkedSpam($isMarkedSpam);
        parent::__construct($createdUser);
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return UserPostHide
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set post
     *
     * @param \Answeredby\EntityBundle\Entity\Post $post
     * @return UserPostHide
     */
    protected function _setPost(\Answeredby\EntityBundle\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \Answeredby\EntityBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set is_marked_spam
     *
     * @param boolean $isMarkedSpam
     * @return UserPostHide
     */
    protected function _setIsMarkedSpam($isMarkedSpam)
    {
        $this->is_marked_spam = $isMarkedSpam;

        return $this;
    }

    /**
     * Get is_marked_spam
     *
     * @return boolean 
     */
    public function getIsMarkedSpam()
    {
        return $this->is_marked_spam;
    }
}
