<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\UserPost;

class UserPostController extends Controller
{
    public function postAction()
    {
        if( !empty($_POST) ){
            $entityManager = $this->getDoctrine()->getManager();
            $userPost = new UserPost($this->getUser(),
                                     $entityManager->getReference('AnsweredbyEntityBundle:Post', filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT)));
            $entityManager->persist($userPost);
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('success' => 0, 'error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            return new Response(json_encode(array('success' => 1, 'userPost' => $userPost->objectify())), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('success' => 0)), 400, array('Content-Type'=>'application/json'));
    }

    public function deleteAction($post_id)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $userPost = $this->getDoctrine()
                ->getRepository('AnsweredbyEntityBundle:UserPost')
                ->createQueryBuilder('userPost')
                ->where('userPost.post = :post_id')
                ->andWhere('userPost.createdUser = :createdUser')
                ->setParameter('post_id', $post_id)
                ->setParameter('createdUser', $this->getUser())
                ->getQuery()
                ->getSingleResult();
        foreach( $userPost->getNotifications() AS $notification )
            $entityManager->remove($notification);
        $entityManager->remove($userPost);
        $entityManager->flush();
        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }

    public function getAction($post_id)
    {
        $userPosts = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:UserPost')->findByPost($post_id);
        $userPostStdClasses = array();
        foreach( $userPosts AS $userPost ){
            $userPostStdClass = new \stdClass();
            $userPostStdClass->userId = $userPost->getCreatedUser()->getId();
            $userPostStdClass->userUsername = $userPost->getCreatedUser()->getUsername();
            $userPostStdClass->userImageUrl = $userPost->getCreatedUser()->getProfilePicUrl();
            $userPostStdClass->userBackgroundHtmlColor = $userPost->getCreatedUser()->getBackgroundHtmlColor();
            $userPostStdClass->userUrl = $this->generateUrl('UserPage', array('userid' => $userPost->getCreatedUser()->getId(), 'username' => $userPost->getCreatedUser()->getUrlUsername()));
            $userPostStdClasses[] = $userPostStdClass;
        }
        return new Response(json_encode(array('userPosts' => $userPostStdClasses)), 200, array('Content-Type'=>'application/json'));
    }
}