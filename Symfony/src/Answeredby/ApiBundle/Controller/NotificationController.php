<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class NotificationController extends Controller
{
    public function postDropdownViewedAction()
    {
        $entityManager = $this->getDoctrine()->getManager();

        foreach( $this->container->get('answeredby.helper.twigGlobals')->getUsersUnviewedNotifications(10) AS $notification ){
            $notification->setViewed(TRUE);
            $entityManager->persist($notification);
        }

        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {
            return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
    }
}
