<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="invitations")
 */
class Invitation extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="md5", type="string", length=255, unique=true)
     */
    protected $md5;

    /**
     * @ORM\OneToMany(targetEntity="UserInvitation", mappedBy="invitation")
     **/
    protected $userInvitations;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="invitation")
     **/
    protected $destinationUser;

    /**
     * @ORM\Column(name="isUnsubscribed", type="boolean")
     */
    protected $isUnsubscribed;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="invitations")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=true)
     */
    protected $createdUser;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->_setMd5($this->getRandomToken());
        $this->setIsUnsubscribed(FALSE);
        $this->userInvitations = new \Doctrine\Common\Collections\ArrayCollection();

        parent::__construct($createdUser);
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Invitation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set md5
     *
     * @param string $md5
     * @return Invitation
     */
    protected function _setMd5($md5)
    {
        $this->md5 = $md5;

        return $this;
    }

    /**
     * Get md5
     *
     * @return string 
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isUnsubscribed
     *
     * @param boolean $isUnsubscribed
     * @return Invitation
     */
    public function setIsUnsubscribed($isUnsubscribed)
    {
        $this->isUnsubscribed = $isUnsubscribed;

        return $this;
    }

    /**
     * Get isUnsubscribed
     *
     * @return boolean 
     */
    public function getIsUnsubscribed()
    {
        return $this->isUnsubscribed;
    }

    /**
     * Add userInvitations
     *
     * @param \Answeredby\EntityBundle\Entity\UserInvitation $userInvitations
     * @return Invitation
     */
    public function addUserInvitation(\Answeredby\EntityBundle\Entity\UserInvitation $userInvitations)
    {
        $this->userInvitations[] = $userInvitations;

        return $this;
    }

    /**
     * Remove userInvitations
     *
     * @param \Answeredby\EntityBundle\Entity\UserInvitation $userInvitations
     */
    public function removeUserInvitation(\Answeredby\EntityBundle\Entity\UserInvitation $userInvitations)
    {
        $this->userInvitations->removeElement($userInvitations);
    }

    /**
     * Get userInvitations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserInvitations()
    {
        return $this->userInvitations;
    }

    /**
     * Get userInvitations with the "isHiddenByInvitationDestinationUser" flag set to false
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNonHiddenUserInvitations()
    {
        return $this->getUserInvitations()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("isHiddenByInvitationDestinationUser", FALSE)));
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Invitation
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        return $this->setCreatedUser($createdUser);
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Invitation
     */
    public function setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser = NULL)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set destinationUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $destinationUser
     */
    public function setDestinationUser(\Answeredby\EntityBundle\Entity\User $destinationUser = NULL)
    {
        $this->destinationUser = $destinationUser;
    }

    /**
     * Get destinationUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getDestinationUser()
    {
        return $this->destinationUser;
    }
}
