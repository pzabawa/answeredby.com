<?php

namespace Answeredby\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RelationshipApproveIgnoreController extends Controller
{
    public function indexAction($md5, $actionType)
    {
        $relationship = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Relationship')->findOneByMd5($md5);
        if( !$relationship )
            return $this->render('AnsweredbyViewBundle:RelationshipApproveIgnore:notfound.html.twig');

        if( $actionType == 'approve' ){
            if( $relationship->getDestinationUser() == $relationship->getCreatedUser() )
                $relationship->setApprovedByOriginationUser(TRUE);
            else
                $relationship->setApprovedByDestinationUser(TRUE);
        } else if( $actionType == 'ignore' ){
            $relationship->setIgnoredByNonCreatedUser(TRUE);
        } else {
            throw $this->createNotFoundException("Invalid action.");
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($relationship);
        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {}

        return $this->render('AnsweredbyViewBundle:RelationshipApproveIgnore:index.html.twig', array('actionType' => $actionType));
    }
}
