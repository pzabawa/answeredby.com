<?php

namespace Answeredby\EntityBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(name="md5", type="string", length=255)
     */
    protected $md5;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    protected $salt;

    /**
     * @ORM\Column(name="password", type="string", length=255)
     */
    protected $password;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    protected $isActive;

    /**
     * @ORM\Column(name="is_admin", type="boolean")
     */
    protected $isAdmin;

    /**
     * @ORM\Column(name="isEmailNotificationRelationshipRequestsSet", type="boolean")
     */
    protected $isEmailNotificationRelationshipRequestsSet;

    /**
     * @ORM\Column(name="isEmailNotificationUpvotesSet", type="boolean")
     */
    protected $isEmailNotificationUpvotesSet;

    /**
     * @ORM\Column(name="isEmailNotificationSharesSet", type="boolean")
     */
    protected $isEmailNotificationSharesSet;

    /**
     * @ORM\Column(name="isEmailNotificationSubpostsSet", type="boolean")
     */
    protected $isEmailNotificationSubpostsSet;

    /**
     * @ORM\Column(name="showTodoPage1", type="boolean")
     */
    protected $showTodoPage1;

    /**
     * @ORM\Column(name="isTodoPage2done", type="boolean")
     */
    protected $isTodoPage2done;

    /**
     * @ORM\Column(name="isTodoPage3done", type="boolean")
     */
    protected $isTodoPage3done;

    /**
     * @ORM\Column(name="isTodoPage5done", type="boolean")
     */
    protected $isTodoPage5done;

    /**
     * @ORM\Column(name="profile_pic_url", type="string", length=255, nullable=true)
     */
    protected $profile_pic_url;

    /**
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    protected $birthday;

    /**
     * @ORM\Column(name="background_html_color", type="string", length=6)
     */
    protected $background_html_color;

    /**
     * @ORM\Column(name="privacySetting", type="integer", nullable=false, options={"default" = 0})
     */
    protected $privacySetting;

    /**
     * @ORM\Column(name="showToDoList", type="boolean")
     */
    protected $showToDoList;

    /**
     * @ORM\Column(name="serializedDefaultPostShareNetworkIds", type="string", length=1000)
     */
    protected $serializedDefaultPostShareNetworkIds;

    /**
     * @ORM\Column(name="serializedDefaultPostShareFriendUsernames", type="string", length=1000)
     */
    protected $serializedDefaultPostShareFriendUsernames;

    /**
     * @ORM\Column(name="createdDatetime", type="datetime")
     */
    protected $createdDatetime;

    /**
     * @ORM\OneToMany(targetEntity="Network", mappedBy="createdUser", cascade={"persist","remove"})
     **/
    protected $networks;

    /**
     * @ORM\OneToMany(targetEntity="Relationship", mappedBy="originationUser", cascade={"remove"})
     **/
    protected $users_relationships;

    /**
     * @ORM\OneToMany(targetEntity="Relationship", mappedBy="destination_user", cascade={"remove"})
     **/
    protected $relationships_with_user;

    /**
     * @ORM\OneToMany(targetEntity="UserPost", mappedBy="createdUser", cascade={"remove"})
     **/
    protected $userPosts;

    /**
     * @ORM\OneToMany(targetEntity="Conversation", mappedBy="createdUser", cascade={"remove"})
     * @ORM\OrderBy({"createdDatetime" = "DESC"})
     **/
    protected $users_conversations;

    /**
     * @ORM\OneToMany(targetEntity="Conversation", mappedBy="destination_user", cascade={"remove"})
     * @ORM\OrderBy({"createdDatetime" = "DESC"})
     **/
    protected $conversations_with_user;

    /**
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="destination_user", cascade={"remove"})
     * @ORM\OrderBy({"createdDatetime" = "DESC"})
     **/
    protected $notifications;

    /**
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="createdUser", cascade={"remove"})
     **/
    protected $createdNotifications;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="createdUser", cascade={"remove"})
     * @ORM\OrderBy({"createdDatetime" = "DESC"})
     **/
    protected $posts;

    /**
     * @ORM\OneToMany(targetEntity="UserInvitation", mappedBy="createdUser", cascade={"remove"})
     **/
    protected $userInvitations;

    /**
     * @ORM\OneToOne(targetEntity="Invitation", mappedBy="destinationUser")
     */
    protected $invitation;

    /**
     * @ORM\OneToMany(targetEntity="Invitation", mappedBy="createdUser")
     **/
    protected $invitations;

    /**
     * @ORM\OneToMany(targetEntity="PostShare", mappedBy="createdUser", cascade={"remove"})
     */
    protected $postShares;

    /**
     * @ORM\OneToMany(targetEntity="PostShareDestinationUser", mappedBy="destinationUser", cascade={"remove"})
     */
    protected $postShareDestinationUsers;

    /**
     * @ORM\OneToMany(targetEntity="ForgottenCredentialsRequest", mappedBy="createdUser", cascade={"remove"})
     */
    protected $forgottenCredentialsRequests;

    /**
     * @ORM\OneToMany(targetEntity="Photo", mappedBy="createdUser", cascade={"remove"})
     */
    protected $photos;

    public function __construct()
    {
        $this->isActive = TRUE;
        $this->isAdmin = FALSE;
        $this->md5 = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->setPrivacySetting(3); // Everyone can search/see profile.
        $this->setCreatedDatetime(new \DateTime());

        $this->setIsEmailNotificationRelationshipRequestsSet(TRUE);
        $this->setIsEmailNotificationSharesSet(TRUE);
        $this->setIsEmailNotificationSubpostsSet(TRUE);
        $this->setIsEmailNotificationUpvotesSet(TRUE);
        $this->setShowTodoPage1(FALSE);
        $this->setShowToDoList(TRUE);
        $this->setIsTodoPage2Done(FALSE);
        $this->setIsTodoPage3Done(FALSE);
        $this->setIsTodoPage5Done(FALSE);
        $this->setSerializedDefaultPostShareNetworkIds(FALSE);
        $this->setSerializedDefaultPostShareFriendUsernames(FALSE);

        $this->postShareDestinationUsers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->invitations = new \Doctrine\Common\Collections\ArrayCollection();

        $individualSharesNetwork = new Network($this);
        $individualSharesNetwork->setText("Individual Shares");
        $individualSharesNetwork->setIsIndividualSharesType(TRUE);
        $this->addNetwork($individualSharesNetwork);

        $friendsNetwork = new Network($this);
        $friendsNetwork->setText("Friends");
        $friendsNetwork->setIsDefaultType(TRUE);
        $this->addNetwork($friendsNetwork);

        // Generate random HTML background colors.
        for( $x = 0; $x < 3; $x++ ){
            $rand = rand(0,255);
            $hex[$x] = sprintf("%x", $rand);
            if( $rand <= 9 || ($rand > 9 && $rand < 16) ){
                $hex[$x] = "0" . $hex[$x];
            }
        }
        $this->setBackgroundHtmlColor($hex[0] . $hex[1] . $hex[2]);
    }

    public function getRoles()
    {
        if( $this->getIsAdmin() )
            return array('ROLE_ADMIN');
        return array('ROLE_USER');
    }

    public function equals(UserInterface $user)
    {
        return $user->getUsername() === $this->username;
    }

    public function eraseCredentials()
    {
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Get username
     *
     * @return string 
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set md5
     *
     * @param string $md5
     */
    public function setMd5($md5)
    {
        $this->md5 = $md5;
    }

    /**
     * Get md5
     *
     * @return string 
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * Set salt
     *
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Get salt
     *
     * @return string 
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set email
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
    }

    /**
     * Get isAdmin
     *
     * @return boolean 
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Set isEmailNotificationRelationshipRequestsSet
     *
     * @param boolean $isEmailNotificationRelationshipRequestsSet
     */
    public function setIsEmailNotificationRelationshipRequestsSet($isEmailNotificationRelationshipRequestsSet)
    {
        $this->isEmailNotificationRelationshipRequestsSet = $isEmailNotificationRelationshipRequestsSet;
    }

    /**
     * Get isEmailNotificationRelationshipRequestsSet
     *
     * @return boolean 
     */
    public function getIsEmailNotificationRelationshipRequestsSet()
    {
        return $this->isEmailNotificationRelationshipRequestsSet;
    }

    /**
     * Set isEmailNotificationUpvotesSet
     *
     * @param boolean $isEmailNotificationUpvotesSet
     */
    public function setIsEmailNotificationUpvotesSet($isEmailNotificationUpvotesSet)
    {
        $this->isEmailNotificationUpvotesSet = $isEmailNotificationUpvotesSet;
    }

    /**
     * Get isEmailNotificationUpvotesSet
     *
     * @return boolean 
     */
    public function getIsEmailNotificationUpvotesSet()
    {
        return $this->isEmailNotificationUpvotesSet;
    }

    /**
     * Set isEmailNotificationSharesSet
     *
     * @param boolean $isEmailNotificationSharesSet
     */
    public function setIsEmailNotificationSharesSet($isEmailNotificationSharesSet)
    {
        $this->isEmailNotificationSharesSet = $isEmailNotificationSharesSet;
    }

    /**
     * Get isEmailNotificationSharesSet
     *
     * @return boolean 
     */
    public function getIsEmailNotificationSharesSet()
    {
        return $this->isEmailNotificationSharesSet;
    }

    /**
     * Set isEmailNotificationSubpostsSet
     *
     * @param boolean $isEmailNotificationSubpostsSet
     */
    public function setIsEmailNotificationSubpostsSet($isEmailNotificationSubpostsSet)
    {
        $this->isEmailNotificationSubpostsSet = $isEmailNotificationSubpostsSet;
    }

    /**
     * Get isEmailNotificationSubpostsSet
     *
     * @return boolean 
     */
    public function getIsEmailNotificationSubpostsSet()
    {
        return $this->isEmailNotificationSubpostsSet;
    }

    /**
     * Set showTodoPage1
     *
     * @param boolean $showTodoPage1
     */
    public function setShowTodoPage1($showTodoPage1)
    {
        $this->showTodoPage1 = $showTodoPage1;
    }

    /**
     * Get showTodoPage1
     *
     * @return boolean 
     */
    public function getShowTodoPage1()
    {
        return $this->showTodoPage1;
    }

    /**
     * Set isTodoPage2done
     *
     * @param boolean $isTodoPage2done
     */
    public function setIsTodoPage2Done($isTodoPage2done)
    {
        $this->isTodoPage2done = $isTodoPage2done;
    }

    /**
     * Get isTodoPage2done
     *
     * @return boolean 
     */
    public function getIsTodoPage2Done()
    {
        return $this->isTodoPage2done;
    }

    /**
     * Set isTodoPage3done
     *
     * @param boolean $isTodoPage3done
     */
    public function setIsTodoPage3Done($isTodoPage3done)
    {
        $this->isTodoPage3done = $isTodoPage3done;
    }

    /**
     * Get isTodoPage3done
     *
     * @return boolean 
     */
    public function getIsTodoPage3Done()
    {
        return $this->isTodoPage3done;
    }

    /**
     * Set isTodoPage5done
     *
     * @param boolean $isTodoPage5done
     */
    public function setIsTodoPage5Done($isTodoPage5done)
    {
        $this->isTodoPage5done = $isTodoPage5done;
    }

    /**
     * Get isTodoPage5done
     *
     * @return boolean 
     */
    public function getIsTodoPage5Done()
    {
        return $this->isTodoPage5done;
    }

    /**
     * Set birthday
     *
     * @param datetime $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * Get birthday
     *
     * @return datetime
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set showToDoList
     *
     * @param boolean $showToDoList
     */
    public function setShowToDoList($showToDoList)
    {
        $this->showToDoList = $showToDoList;
    }

    /**
     * Get showToDoList
     *
     * @return boolean 
     */
    public function getShowToDoList()
    {
        return $this->showToDoList;
    }


    /**
     * Set serializedDefaultPostShareNetworkIds
     *
     * @param string $serializedDefaultPostShareNetworkIds
     */
    public function setSerializedDefaultPostShareNetworkIds($serializedDefaultPostShareNetworkIds)
    {
        $this->serializedDefaultPostShareNetworkIds = $serializedDefaultPostShareNetworkIds;
    }

    /**
     * Get serializedDefaultPostShareNetworkIds
     *
     * @return string 
     */
    public function getSerializedDefaultPostShareNetworkIds()
    {
        return $this->serializedDefaultPostShareNetworkIds;
    }

    /**
     * Get serializedDefaultPostShareNetworkIds unserialized and json_encoded
     *
     * @return string 
     */
    public function getSerializedDefaultPostShareNetworkIdsUnserializedAndJsonEncoded()
    {
        return json_encode(unserialize($this->getSerializedDefaultPostShareNetworkIds()));
    }

    /**
     * Set serializedDefaultPostShareFriendUsernames
     *
     * @param string $serializedDefaultPostShareFriendUsernames
     */
    public function setSerializedDefaultPostShareFriendUsernames($serializedDefaultPostShareFriendUsernames)
    {
        $this->serializedDefaultPostShareFriendUsernames = $serializedDefaultPostShareFriendUsernames;
    }

    /**
     * Get serializedDefaultPostShareFriendUsernames
     *
     * @return string 
     */
    public function getSerializedDefaultPostShareFriendUsernames()
    {
        return $this->serializedDefaultPostShareFriendUsernames;
    }

    /**
     * Get serializedDefaultPostShareFriendUsernames unserialized and json_encoded
     *
     * @return string 
     */
    public function getSerializedDefaultPostShareFriendUsernamesUnserializedAndJsonEncoded()
    {
        return json_encode(unserialize($this->getSerializedDefaultPostShareFriendUsernames()));
    }

    /**
     * Add networks
     *
     * @param \Answeredby\EntityBundle\Entity\Network $networks
     * @return User
     */
    public function addNetwork(\Answeredby\EntityBundle\Entity\Network $networks)
    {
        $this->networks[] = $networks;

        return $this;
    }

    /**
     * Remove networks
     *
     * @param \Answeredby\EntityBundle\Entity\Network $networks
     */
    public function removeNetwork(\Answeredby\EntityBundle\Entity\Network $networks)
    {
        $this->networks->removeElement($networks);
    }

    /**
     * Get networks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNetworks()
    {
        return $this->networks;
    }

    /**
     * Get non-hidden networks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNonHiddenNetworks()
    {
        return $this->networks->matching(Criteria::create()
                ->where(Criteria::expr()->eq("isHidden", FALSE)));
    }

    /**
     * Get networks without the "is_individual_shares_type" flag
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNetworksWithoutIsIndividualSharesType()
    {
        return $this->networks->matching(Criteria::create()
                ->where(Criteria::expr()->eq("isIndividualSharesType", FALSE))
                ->andWhere(Criteria::expr()->eq("isHidden", FALSE)));
    }

    /**
     * Get networks with the "is_individual_shares_type" flag
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNetworksWhereIsIndividualSharesType()
    {
        return $this->networks->matching(Criteria::create()
                ->where(Criteria::expr()->eq("isIndividualSharesType", TRUE)));
    }

    /**
     * Add users_relationships
     *
     * @param \Answeredby\EntityBundle\Entity\Relationship $usersRelationships
     * @return User
     */
    public function addUsersRelationship(\Answeredby\EntityBundle\Entity\Relationship $usersRelationships)
    {
        $this->users_relationships[] = $usersRelationships;

        return $this;
    }

    /**
     * Remove users_relationships
     *
     * @param \Answeredby\EntityBundle\Entity\Relationship $usersRelationships
     */
    public function removeUsersRelationship(\Answeredby\EntityBundle\Entity\Relationship $usersRelationships)
    {
        $this->users_relationships->removeElement($usersRelationships);
    }

    /**
     * Get users_relationships
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsersRelationships()
    {
        return $this->users_relationships;
    }

    /**
     * Get users_relationships with a specific User "approvedByOriginationUser" 
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsersRelationshipsWithSpecificUserApprovedByOriginationUser(\Answeredby\EntityBundle\Entity\User $relationshipsWithUserUser)
    {
        return $this->getUsersRelationships()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("destination_user", $relationshipsWithUserUser))
                ->andWhere(Criteria::expr()->eq("approvedByOriginationUser", TRUE)));
    }

    /**
     * Get users_relationships with a specific User "approvedByOriginationUser" for a specific Network
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsersRelationshipsWithSpecificUserAndNetworkApprovedByOriginationUser(\Answeredby\EntityBundle\Entity\User $relationshipsWithUserUser, \Answeredby\EntityBundle\Entity\Network $network)
    {
        return $this->getUsersRelationships()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("destination_user", $relationshipsWithUserUser))
                ->andWhere(Criteria::expr()->eq("network", $network))
                ->andWhere(Criteria::expr()->eq("approvedByOriginationUser", TRUE)));
    }

    /**
     * Get users_relationships with a specific User "approvedByDestinationUser" 
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsersRelationshipsWithSpecificUserApprovedByDestinationUser(\Answeredby\EntityBundle\Entity\User $relationshipsWithUserUser)
    {
        return $this->getUsersRelationships()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("destination_user", $relationshipsWithUserUser))
                ->andWhere(Criteria::expr()->eq("approvedByDestinationUser", TRUE)));
    }

    /**
     * Get users_relationships with a specific User "approvedByDestinationUser" for a specific Network
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsersRelationshipsWithSpecificUserAndNetworkApprovedByDestinationUser(\Answeredby\EntityBundle\Entity\User $relationshipsWithUserUser, \Answeredby\EntityBundle\Entity\Network $network)
    {
        return $this->getUsersRelationships()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("destination_user", $relationshipsWithUserUser))
                ->andWhere(Criteria::expr()->eq("network", $network))
                ->andWhere(Criteria::expr()->eq("approvedByDestinationUser", TRUE)));
    }

    /**
     * Get users_relationships with a specific User "approvedByDestinationUser" 
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsersRelationshipsWithSpecificUserApprovedByBothUser(\Answeredby\EntityBundle\Entity\User $relationshipsWithUserUser)
    {
        return $this->getUsersRelationships()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("destination_user", $relationshipsWithUserUser))
                ->andWhere(Criteria::expr()->eq("approvedByOriginationUser", TRUE))
                ->andWhere(Criteria::expr()->eq("approvedByDestinationUser", TRUE)));
    }

    /**
     * Add relationships_with_user
     *
     * @param \Answeredby\EntityBundle\Entity\Relationship $relationshipsWithUser
     * @return User
     */
    public function addRelationshipsWithUser(\Answeredby\EntityBundle\Entity\Relationship $relationshipsWithUser)
    {
        $this->relationships_with_user[] = $relationshipsWithUser;

        return $this;
    }

    /**
     * Remove relationships_with_user
     *
     * @param \Answeredby\EntityBundle\Entity\Relationship $relationshipsWithUser
     */
    public function removeRelationshipsWithUser(\Answeredby\EntityBundle\Entity\Relationship $relationshipsWithUser)
    {
        $this->relationships_with_user->removeElement($relationshipsWithUser);
    }

    /**
     * Get relationships_with_user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelationshipsWithUser()
    {
        return $this->relationships_with_user;
    }

    /**
     * Add posts
     *
     * @param \Answeredby\EntityBundle\Entity\Post $posts
     * @return User
     */
    public function addPost(\Answeredby\EntityBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;

        return $this;
    }

    /**
     * Remove posts
     *
     * @param \Answeredby\EntityBundle\Entity\Post $posts
     */
    public function removePost(\Answeredby\EntityBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * Set profile_pic_url
     *
     * @param string $profilePicUrl
     * @return User
     */
    public function setProfilePicUrl($profilePicUrl)
    {
        $this->profile_pic_url = $profilePicUrl;

        return $this;
    }

    /**
     * Get profile_pic_url
     *
     * @return string 
     */
    public function getProfilePicUrl()
    {
        if( $this->profile_pic_url )
            return $this->profile_pic_url;
        return '/images/silhouette.png';
    }

    /**
     * Get if profile_pic_url is not empty
     *
     * @return boolean 
     */
    public function hasSetProfilePicUrl()
    {
        return $this->profile_pic_url ? TRUE : FALSE;
    }

    /**
     * Set background_html_color
     *
     * @param string $background_html_color
     * @return User
     */
    public function setBackgroundHtmlColor($background_html_color)
    {
        $this->background_html_color = $background_html_color;
    
        return $this;
    }

    /**
     * Get background_html_color
     *
     * @return string 
     */
    public function getBackgroundHtmlColor()
    {
        return $this->background_html_color;
    }

    /**
     * Set privacySetting
     *
     * @param integer $privacySetting
     * @return User
     */
    public function setPrivacySetting($privacySetting)
    {
        $this->privacySetting = $privacySetting;
    
        return $this;
    }

    /**
     * Get privacySetting
     *
     * @return string 
     */
    public function getPrivacySetting()
    {
        return $this->privacySetting;
    }

    public function toStdClass()
    {
        $javascriptObject = new \stdClass();
        $javascriptObject->id = $this->getId();
        $javascriptObject->username = $this->getUsername();
        $javascriptObject->imageUrl = $this->getProfilePicUrl();
        $javascriptObject->backgroundHtmlColor = $this->getBackgroundHtmlColor();
        return $javascriptObject;
    }

    /**
     * Add userPosts
     *
     * @param \Answeredby\EntityBundle\Entity\UserPost $userPosts
     * @return User
     */
    public function addUserPost(\Answeredby\EntityBundle\Entity\UserPost $userPosts)
    {
        $this->userPosts[] = $userPosts;

        return $this;
    }

    /**
     * Remove userPosts
     *
     * @param \Answeredby\EntityBundle\Entity\UserPost $userPosts
     */
    public function removeUserPost(\Answeredby\EntityBundle\Entity\UserPost $userPosts)
    {
        $this->userPosts->removeElement($userPosts);
    }

    /**
     * Get userPosts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserPosts()
    {
        return $this->userPosts;
    }

    /**
     * Get userPosts with post $post
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserPostsWithPost($post)
    {
        return $this->getUserPosts()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("post", $post)));
    }

    /**
     * Add notifications
     *
     * @param \Answeredby\EntityBundle\Entity\Notification $notifications
     * @return User
     */
    public function addNotification(\Answeredby\EntityBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \Answeredby\EntityBundle\Entity\Notification $notifications
     */
    public function removeNotification(\Answeredby\EntityBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Add users_conversations
     *
     * @param \Answeredby\EntityBundle\Entity\Conversation $users_conversations
     * @return User
     */
    public function addUsersConversation(\Answeredby\EntityBundle\Entity\Conversation $users_conversations)
    {
        $this->users_conversations[] = $users_conversations;

        return $this;
    }

    /**
     * Remove users_conversations
     *
     * @param \Answeredby\EntityBundle\Entity\Conversation $users_conversations
     */
    public function removeUsersConversation(\Answeredby\EntityBundle\Entity\Conversation $users_conversations)
    {
        $this->users_conversations->removeElement($users_conversations);
    }

    /**
     * Get users_conversations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsersConversations()
    {
        return $this->users_conversations;
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return User
     */
    public function setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;
    
        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    /**
     * Add conversations_with_user
     *
     * @param \Answeredby\EntityBundle\Entity\Conversation $conversationsWithUser
     * @return User
     */
    public function addConversationsWithUser(\Answeredby\EntityBundle\Entity\Conversation $conversationsWithUser)
    {
        $this->conversations_with_user[] = $conversationsWithUser;

        return $this;
    }

    /**
     * Remove conversations_with_user
     *
     * @param \Answeredby\EntityBundle\Entity\Conversation $conversationsWithUser
     */
    public function removeConversationsWithUser(\Answeredby\EntityBundle\Entity\Conversation $conversationsWithUser)
    {
        $this->conversations_with_user->removeElement($conversationsWithUser);
    }

    /**
     * Get conversations_with_user
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConversationsWithUser()
    {
        return $this->conversations_with_user;
    }

    /**
     * Get conversations_with_user and users_conversations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAllConversations()
    {
        return array_merge($this->getConversationsWithUser()->toArray(), $this->getUsersConversations()->toArray());
    }

    public function getUnopenedMessagesCount()
    {
        $unopenedMessagesCount = 0;
        foreach( $this->getAllConversations() AS $conversation )
            $unopenedMessagesCount += count($conversation->getUnopenedMessagesNotByUser($this));
        return $unopenedMessagesCount;
    }

    /**
     * Add userInvitations
     *
     * @param \Answeredby\EntityBundle\Entity\UserInvitation $userInvitations
     * @return User
     */
    public function addUserInvitation(\Answeredby\EntityBundle\Entity\UserInvitation $userInvitations)
    {
        $this->userInvitations[] = $userInvitations;

        return $this;
    }

    /**
     * Remove userInvitations
     *
     * @param \Answeredby\EntityBundle\Entity\UserInvitation $userInvitations
     */
    public function removeUserInvitation(\Answeredby\EntityBundle\Entity\UserInvitation $userInvitations)
    {
        $this->userInvitations->removeElement($userInvitations);
    }

    /**
     * Get userInvitations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUserInvitations()
    {
        return $this->userInvitations;
    }

    /**
     * Set invitation
     *
     * @param \Answeredby\EntityBundle\Entity\Invitation $invitation
     */
    public function setInvitation(\Answeredby\EntityBundle\Entity\Invitation $invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Get invitation
     *
     * @return \Answeredby\EntityBundle\Entity\Invitation 
     */
    public function getInvitation()
    {
        return $this->invitation;
    }

    /**
     * Add invitations
     *
     * @param \Answeredby\EntityBundle\Entity\Invitation $invitations
     * @return User
     */
    public function addInvitation(\Answeredby\EntityBundle\Entity\Invitation $invitations)
    {
        $this->invitations[] = $invitations;

        return $this;
    }

    /**
     * Remove invitations
     *
     * @param \Answeredby\EntityBundle\Entity\Invitation $invitations
     */
    public function removeInvitation(\Answeredby\EntityBundle\Entity\Invitation $invitations)
    {
        $this->invitations->removeElement($invitations);
    }

    /**
     * Get invitations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getInvitations()
    {
        return $this->invitations;
    }

    /**
     * Add postShareDestinationUsers
     *
     * @param \Answeredby\EntityBundle\Entity\PostShareDestinationUser $postShareDestinationUser
     * @return User
     */
    public function addPostShareDestinationUser(\Answeredby\EntityBundle\Entity\PostShareDestinationUser $postShareDestinationUser)
    {
        $this->postShareDestinationUsers[] = $postShareDestinationUser;

        return $this;
    }

    /**
     * Remove postShareDestinationUsers
     *
     * @param \Answeredby\EntityBundle\Entity\PostShareDestinationUser $postShareDestinationUser
     */
    public function removePostShareDestinationUser(\Answeredby\EntityBundle\Entity\PostShareDestinationUser $postShareDestinationUser)
    {
        $this->postShareDestinationUsers->removeElement($postShareDestinationUser);
    }

    /**
     * Get postShareDestinationUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPostShareDestinationUsers()
    {
        return $this->postShareDestinationUsers;
    }

    public function getUrlUsername()
    {
        $username = html_entity_decode($this->getUsername(), ENT_QUOTES);
        $username = preg_replace("/[^A-Za-z0-9 ]/", "", $username);
        $username = str_replace(" ", "+", $username);
        return $username;
    }
}
