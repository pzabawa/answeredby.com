<?php

namespace Answeredby\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EmailSubscriptionsController extends Controller
{
    public function indexAction($md5Type, $md5)
    {
        if( $md5Type == 'notification' ){

            $notification = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Notification')->findOneByMd5($md5);
            if( !$notification )
                throw $this->createNotFoundException("No notification found.");
            return $this->render('AnsweredbyViewBundle:EmailSubscriptions:index.html.twig', array('id_type' => 'notification_id', 'md5' => $md5, 'user' => $notification->getDestinationUser()));

        } else if( $md5Type == 'relationship' ){

            $relationship = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Relationship')->findOneByMd5($md5);
            if( !$relationship )
                throw $this->createNotFoundException("No relationship found.");
            return $this->render('AnsweredbyViewBundle:EmailSubscriptions:index.html.twig', array('id_type' => 'relationship_id', 'md5' => $md5, 'user' => $relationship->getNonCreatedUser()));

        } else {
            throw $this->createNotFoundException("Invalid md5 type.");
        }
    }
}
