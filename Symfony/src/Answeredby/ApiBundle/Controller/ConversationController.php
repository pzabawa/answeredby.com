<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\Conversation;
use Answeredby\EntityBundle\Entity\Message;

class ConversationController extends Controller
{
    public function postAction()
    {
        if( !empty($_POST) ){
            $conversation = new Conversation($this->getUser());

            $recipientUsernames = filter_input(INPUT_POST, 'recipient_username_array', FILTER_SANITIZE_STRING, FILTER_REQUIRE_ARRAY);
            if( !empty($recipientUsernames) ){
                foreach( $recipientUsernames AS $recipientUsername ){
                    $destinationUser = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->findOneByUsername($recipientUsername);
                    if( $destinationUser )
                        $conversation->setDestinationUser($destinationUser);
                    else
                        return new Response(json_encode(array('error' => "No user found for username $recipientUsername.")), 400, array('Content-Type'=>'application/json'));
                }
            }

            $message = new Message($this->getUser(),
                                   $conversation);
            $message->updateFromPostArray($_POST);
            $message->addAnchorTagsToTextUrls();
            $message->replaceBackslashNCharacatersWithBrTagsInText();
            $conversation->addMessage($message);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($conversation);
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            $destinationUserStdClass = new \stdClass();
            $destinationUserStdClass->username = $destinationUser->getUsername();
            $destinationUserStdClass->imageUrl = $destinationUser->getProfilePicUrl();
            $destinationUserStdClass->backgroundHtmlColor = $destinationUser->getBackgroundHtmlColor();
            $destinationUserStdClass->url = $this->generateUrl('UserPage', array('userid' => $destinationUser->getId(), 'username' => $destinationUser->getUrlUsername()));

            return new Response(json_encode(array('text' => $message->getText(), 'conversation_id' => $conversation->getId(), 'destination_user_object' => $destinationUserStdClass, 'createdDatetime' => $message->getCreatedDatetime()->format('Y-m-d\\TH:i:sP'))), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }

    public function postOpenedAction()
    {
        if( !empty($_POST) ){
            $conversation = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:Conversation')->findOneById(filter_input(INPUT_POST, 'conversation_id', FILTER_SANITIZE_NUMBER_INT));

            if( $conversation && ($conversation->getCreatedUser() == $this->getUser() || $conversation->getDestinationUser() == $this->getUser()) ){
                $entityManager = $this->getDoctrine()->getManager();

                foreach( $conversation->getMessages() AS $message ){
                     $message->setHasDestinationUserOpened(TRUE);
                     $entityManager->persist($message);
                }

                try {
                    $entityManager->flush();
                } catch (\Doctrine\DBAL\DBALException $e) {}

                return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
            }

            return new Response(json_encode(array('error' => 'Inaccessible id.')), 400, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }
}
