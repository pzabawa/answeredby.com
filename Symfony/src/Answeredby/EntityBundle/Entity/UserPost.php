<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="userPosts")
 */
class UserPost extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="userPosts")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="userPosts")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", nullable=false)
     */
    protected $post;

    /**
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="new_userpost", cascade={"persist"})
     **/
    protected $notifications;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser,
                                \Answeredby\EntityBundle\Entity\Post $post)
    {
        $this->notifications = new \Doctrine\Common\Collections\ArrayCollection();

        $this->_setPost($post);
        parent::__construct($createdUser);

        if( $this->getCreatedUser() != $this->getPost()->getCreatedUser() ){
            $notification = new Notification($this->getCreatedUser(),
                                             $this->getPost()->getCreatedUser());
            $notification->setNewUserpost($this);
            $this->addNotification($notification);
        }
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return UserPost
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set post
     *
     * @param \Answeredby\EntityBundle\Entity\Post $post
     * @return UserPost
     */
    protected function _setPost(\Answeredby\EntityBundle\Entity\Post $post)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \Answeredby\EntityBundle\Entity\Post 
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Add notifications
     *
     * @param \Answeredby\EntityBundle\Entity\Notification $notifications
     * @return UserPost
     */
    public function addNotification(\Answeredby\EntityBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \Answeredby\EntityBundle\Entity\Notification $notifications
     */
    public function removeNotification(\Answeredby\EntityBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
}
