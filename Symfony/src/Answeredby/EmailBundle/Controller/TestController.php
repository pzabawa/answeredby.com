<?php

namespace Answeredby\EmailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TestController extends Controller
{
    public function forgotAction()
    {
        return $this->render('AnsweredbyEmailBundle:Forgot:email.html.twig', array('md5' => 'md5'));
    }

    public function invitationAction()
    {
        $invitation = new \Answeredby\EntityBundle\Entity\Invitation($this->getUser());
        $userInvitation = new \Answeredby\EntityBundle\Entity\UserInvitation($this->getUser(), $invitation);
        return $this->render('AnsweredbyEmailBundle:Invitation:email.html.twig', array('userInvitation' => $userInvitation));
    }

    public function notificationAction()
    {
        $parentPost = new \Answeredby\EntityBundle\Entity\Post($this->getUser());
        $parentPost->setId(1);
        $subPost = new \Answeredby\EntityBundle\Entity\Post($this->getUser());
        $subPost->setParentPost($parentPost);
        $notification = new \Answeredby\EntityBundle\Entity\Notification($this->getUser(), $this->getUser());
        $notification->setNewSubpost($subPost);
        return $this->render('AnsweredbyEmailBundle:Notification:email.html.twig', array('notification' => $notification));
    }

    public function relationshipAction()
    {
        $network = new \Answeredby\EntityBundle\Entity\Network($this->getUser());
        $relationship = new \Answeredby\EntityBundle\Entity\Relationship($this->getUser(), $this->getUser(), $this->getUser(), $network);
        $relationships = array($relationship);
        return $this->render('AnsweredbyEmailBundle:Relationship:email.html.twig', array('relationships' => $relationships));
    }
}
