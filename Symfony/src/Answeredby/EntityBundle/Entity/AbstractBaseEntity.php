<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class AbstractBaseEntity
{
    /**
     * @ORM\Column(name="createdDatetime", type="datetime")
     */
    protected $createdDatetime;

    abstract protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser);

    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->_setCreatedUser($createdUser);
        $this->_setCreatedDatetime(new \DateTime());
    }

    /**
     * Set createdDatetime
     *
     * @param \DateTime $createdDatetime
     * @return AbstractBaseEntity
     */
    protected function _setCreatedDatetime($createdDatetime)
    {
        $this->createdDatetime = $createdDatetime;
    
        return $this;
    }

    /**
     * Get createdDatetime
     *
     * @return \DateTime 
     */
    public function getCreatedDatetime()
    {
        return $this->createdDatetime;
    }

    private function _removeNonUrlCharacters($string)
    {
        $text = html_entity_decode($string, ENT_QUOTES);
        $text = preg_replace("/[^A-Za-z0-9 ]/", "", $text);
        $text = str_replace(" ", "+", $text);
        return $text;
    }

    public function updateFromPostArray(array &$post)
    {
        foreach( get_object_vars($this) AS $varName => $varValue )
        {
            if( isset($post[$varName]) ){
                $post[$varName] = filter_var($post[$varName], FILTER_SANITIZE_STRING);
                if( $post[$varName] != 'createdUserId' ){
                    $functionName = 'set' . str_replace(' ', '', ucwords( str_replace('_', ' ', $varName) ));
                    if( method_exists($this, $functionName) )
                        $this->$functionName($post[$varName]);
                }
            }
        }
    }

    public function objectify($toArray = TRUE)
    {
        $objectVarArray = array();
        $objectStdClass = new \stdClass();

        foreach( get_object_vars($this) AS $varName => $varValue )
        {
            if( is_a($varValue, 'Answeredby\EntityBundle\Entity\User') ){
                $objectVarArray[$varName] = array("id" => $varValue->getId(), "username" => $varValue->getUsername());
                $objectStdClass->$varName = array("id" => $varValue->getId(), "username" => $varValue->getUsername());
            } else {
                $functionName = 'get' . str_replace(' ', '', ucwords( str_replace('_', ' ', $varName) ));
                if( $functionName == 'getInitializer' || $functionName == 'getCloner'  || $functionName == 'getIsInitialized')
                    continue;
                $objectVarArray[$varName] = $this->$functionName();
                $objectStdClass->$varName = $this->$functionName();
            }
        }

        if( $toArray )
            return $objectVarArray;
        return $objectStdClass;
    }

    public function addAnchorTagsToTextUrls()
    {
        $text = $this->getText();
        $reg_exUrl = "#(?i)\\b((?:[a-z][\\w-]+:(?:/{1,3}|[a-z0-9%])|www\\d{0,3}[.]|[a-z0-9.\\-]+[.][a-z]{2,4}/)(?:[^\\s()<>]+|\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\))+(?:\\(([^\\s()<>]+|(\\([^\\s()<>]+\\)))*\\)|[^\\s`!()\\[\\]{};:'\".,<>?«»“”‘’]))#";
        preg_match_all($reg_exUrl, $text, $matches);
        $usedPatterns = array();
        foreach($matches[0] as $pattern){
            if(!array_key_exists($pattern, $usedPatterns)){
                $usedPatterns[$pattern]=true;
                $patternWithoutHttp = strtolower(substr($pattern,0,7)) == "http://" ? substr($pattern,7) : $pattern;
                $patternWithoutHttp = strtolower(substr($pattern,0,8)) == "https://" ? substr($pattern,8) : $patternWithoutHttp;
                $text = str_replace($pattern, "<a href='http://{$patternWithoutHttp}' target='_blank'>{$pattern}</a>", $text);
            }
        }
        $this->setText($text);
    }

    public function replaceBackslashNCharacatersWithBrTagsInText()
    {
        $this->setText(str_replace("\n", "<br>", $this->getText()));
    }

    public function divideTextToTextAndHiddenText()
    {
        if( strlen($this->getText()) > 200 ){
            $firstSpaceAfter200CharsPos = strpos($this->getText(), " ", 200);
            if( $firstSpaceAfter200CharsPos ){
                $this->setHiddenText(substr($this->getText(), $firstSpaceAfter200CharsPos));
                $this->setText(substr($this->getText(), 0, $firstSpaceAfter200CharsPos));
            }
        }
    }

    public function getUrlText()
    {
        return $this->_removeNonUrlCharacters($this->getText());
    }

    public function getUrlTitle()
    {
        return $this->_removeNonUrlCharacters($this->getTitle());
    }

    private function _cryptoRandSecure($min, $max)
    {
        $range = $max - $min;
        if ($range < 0) return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    protected function getRandomToken($length=32)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( $i=0; $i<$length; $i++ )
            $token .= $codeAlphabet[$this->_cryptoRandSecure(0,strlen($codeAlphabet))];
        return $token;
    }
}