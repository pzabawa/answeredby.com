<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\UserPostHide;

class UserPostHideController extends Controller
{
    public function postAction()
    {
        if( !empty($_POST) ){
            $entityManager = $this->getDoctrine()->getManager();
            $userPostHide = new UserPostHide($this->getUser(),
                                             $entityManager->getReference('AnsweredbyEntityBundle:Post', filter_input(INPUT_POST, 'post_id', FILTER_SANITIZE_NUMBER_INT)),
                                             filter_input(INPUT_POST, 'is_marked_spam', FILTER_SANITIZE_NUMBER_INT));
            $entityManager->detach($userPostHide);
            $entityManager->merge($userPostHide);
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            return new Response(json_encode(array('success' => 1)), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }
}
