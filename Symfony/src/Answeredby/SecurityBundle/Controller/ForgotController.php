<?php

namespace Answeredby\SecurityBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Answeredby\EntityBundle\Entity\ForgottenCredentialsRequest;
use Mailgun\Mailgun;

class ForgotController extends Controller
{   
    public function indexAction()
    {
        $request = $this->getRequest();
        $form = $this->createFormBuilder()
            ->add('email', 'text', array('label' => 'E-mail Address'))
            ->getForm();

        if( $request->getMethod() == 'POST' ){
            $form->handleRequest($request);

            if( $form->isValid() ){
                $email = filter_var($form->getData(),FILTER_SANITIZE_EMAIL);

                // Get user object.
                $user = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:User')->findOneByEmail($email);

                // Set up forgotCredentialsRequest object.
                if( $user ){
                    $forgottenCredentialsRequest = new ForgottenCredentialsRequest($user);

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($forgottenCredentialsRequest);
                    $entityManager->flush();

                    $mgClient = new Mailgun('key-43c92a7259f73ab1bda5ab3e0d7733c4');
                    $mgClient->sendMessage('answeredby.com', array(
                        'from'    => 'AnsweredBy <' . $this->container->getParameter('mailer_user') . '>',
                        'to'      => $email,
                        'subject' => 'AnsweredBy Password Reset',
                        'text'    => $this->renderView('AnsweredbyEmailBundle:Forgot:email.txt.twig',  array('md5' => $forgottenCredentialsRequest->getMd5())),
                        'html'    => $this->renderView('AnsweredbyEmailBundle:Forgot:email.html.twig', array('md5' => $forgottenCredentialsRequest->getMd5()))
                    ));
                }

                // Prevent brute force attacks by delaying each forgot submission 1 second.
                sleep(1);

                return $this->render('AnsweredbySecurityBundle:Forgot:post.html.twig', array('email' => $email));
            }
        }

        return $this->render('AnsweredbySecurityBundle:Forgot:index.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
