<?php

namespace UrlGrabber\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class UrlGrabberController extends Controller
{
    protected function _file_get_contents_curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 8);
        curl_setopt($ch, CURLOPT_COOKIEFILE, '');

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public function getAction()
    {
        if( array_key_exists('url', $_GET) ){
            $html = $this->_file_get_contents_curl(filter_input(INPUT_GET, 'url', FILTER_SANITIZE_STRING));

            //parsing begins here:
            $doc = new \DOMDocument();
            @$doc->loadHTML('<?xml encoding="UTF-8">' . $html);
            $nodes = $doc->getElementsByTagName('title');

            //get and display what you need:
            $title = $description = $keywords = "";

            if( $nodes && $nodes->item(0) )
                $title = $nodes->item(0)->nodeValue;

            $metas = $doc->getElementsByTagName('meta');

            for ($i = 0; $i < $metas->length; $i++)
            {
                $meta = $metas->item($i);
                if($meta->getAttribute('name') == 'description')
                    $description = $meta->getAttribute('content');
                if($meta->getAttribute('name') == 'keywords')
                    $keywords = $meta->getAttribute('content');
            }

            return new Response(str_replace('<?xml encoding="UTF-8"?>', '', $doc->saveXML()));
        }
        return new Response(json_encode(array('error' => 'No url GET parameter received.')), 400, array('Content-Type'=>'application/json'));
    }
}
