<?php

namespace Answeredby\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Answeredby\EntityBundle\Entity\Message;

class MessageController extends Controller
{
    public function postAction()
    {
        if( !empty($_POST) ){
            $entityManager = $this->getDoctrine()->getManager();

            $message = new Message($this->getUser(),
                                   $entityManager->getReference('AnsweredbyEntityBundle:Conversation', filter_input(INPUT_POST, 'conversation_id', FILTER_SANITIZE_NUMBER_INT)));
            $message->updateFromPostArray($_POST);
            $message->addAnchorTagsToTextUrls();
            $message->replaceBackslashNCharacatersWithBrTagsInText();

            $entityManager->persist($message);
            try {
                $entityManager->flush();
            } catch (\Doctrine\DBAL\DBALException $e) {
                return new Response(json_encode(array('error' => $e->getPrevious()->getCode())), 500, array('Content-Type'=>'application/json'));
            }

            return new Response(json_encode(array('text' => $message->getText(), 'createdDatetime' => $message->getCreatedDatetime()->format('Y-m-d\\TH:i:sP'))), 200, array('Content-Type'=>'application/json'));
        }

        return new Response(json_encode(array('error' => 'No POST data received.')), 400, array('Content-Type'=>'application/json'));
    }
}