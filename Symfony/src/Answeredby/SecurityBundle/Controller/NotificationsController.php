<?php

namespace Answeredby\SecurityBundle\Controller;

use Answeredby\SecurityBundle\Controller\SecurityBundleController;

class NotificationsController extends SecurityBundleController
{   
    public function indexAction()
    {
        if( !$this->_isAuthenticated() )
            return $this->redirect($this->generateUrl('login') . "?target_url=" . $this->generateUrl('NotificationsPage', array(), TRUE));

        $entityManager = $this->getDoctrine()->getManager();

        foreach( $this->container->get('answeredby.helper.twigGlobals')->getUsersNotifications() AS $notification ){
            if( !$notification->getViewed() ){
                $notification->setViewed(TRUE);
                $entityManager->persist($notification);
            }
        }

        try {
            $entityManager->flush();
        } catch (\Doctrine\DBAL\DBALException $e) {}

        return $this->render('AnsweredbySecurityBundle:Notifications:index.html.twig');
    }
}
