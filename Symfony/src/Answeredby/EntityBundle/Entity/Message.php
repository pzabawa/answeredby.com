<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="messages")
 */
class Message extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="text", type="text")
     */
    protected $text;

    /**
     * @ORM\Column(name="has_destination_user_opened", type="boolean")
     */
    protected $has_destination_user_opened;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\ManyToOne(targetEntity="Conversation", inversedBy="messages")
     * @ORM\JoinColumn(name="conversation_id", referencedColumnName="id", nullable=false)
     */
    protected $conversation;

    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser,
                                \Answeredby\EntityBundle\Entity\Conversation $conversation)
    {
        $this->setHasDestinationUserOpened(FALSE);
        $this->_setConversation($conversation);
        parent::__construct($createdUser);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Message
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set conversation
     *
     * @param \Answeredby\EntityBundle\Entity\Conversation $conversation
     * @return Message
     */
    protected function _setConversation(\Answeredby\EntityBundle\Entity\Conversation $conversation)
    {
        $this->conversation = $conversation;

        return $this;
    }

    /**
     * Get conversation
     *
     * @return \Answeredby\EntityBundle\Entity\Conversation 
     */
    public function getConversation()
    {
        return $this->conversation;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Message
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set has_destination_user_opened
     *
     * @param boolean $hasDestinationUserOpened
     * @return Message
     */
    public function setHasDestinationUserOpened($hasDestinationUserOpened)
    {
        $this->has_destination_user_opened = $hasDestinationUserOpened;

        return $this;
    }

    /**
     * Get has_destination_user_opened
     *
     * @return boolean 
     */
    public function getHasDestinationUserOpened()
    {
        return $this->has_destination_user_opened;
    }
}
