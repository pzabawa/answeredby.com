<?php

namespace Answeredby\SecurityBundle\Controller;

use Answeredby\SecurityBundle\Controller\SecurityBundleController;

class ResetController extends SecurityBundleController
{   
    public function indexAction($md5)
    {
        if( empty($md5) && !$this->_isAuthenticated() )
            return $this->redirect($this->generateUrl('login') . "?target_url=" . $this->generateUrl('ResetPage', array(), TRUE));

        $request = $this->getRequest();
        $form = $this->createFormBuilder()
            ->add('password', 'password', array('max_length' => 255))
            ->getForm();

        // If directed to from reset link.
        if( !empty($md5) ){
            $forgottenCredentialsRequest = $this->getDoctrine()->getRepository('AnsweredbyEntityBundle:ForgottenCredentialsRequest')->findOneByMd5($md5);
            if( is_a($forgottenCredentialsRequest, 'Answeredby\EntityBundle\Entity\ForgottenCredentialsRequest') ){
                // Only respect forgot requests created less than a day ago.
                $datetimeinterval = $forgottenCredentialsRequest->getCreatedDatetime()->diff(new \DateTime());
                if( $datetimeinterval->d < 1 )  // Less tha 1 day from sending...
                    $user = $forgottenCredentialsRequest->getCreatedUser();
            }
        }

        // If reset from account page.
        if( is_a($this->getUser(), 'Answeredby\EntityBundle\Entity\User') )
            $user = $this->getUser();

        if( is_a($user, 'Answeredby\EntityBundle\Entity\User') ){
            if( $request->getMethod() == 'POST' ){
                $form->handleRequest($request);

                if( $form->isValid() ){
                    // Encrypt the password.
                    $factory = $this->get('security.encoder_factory');
                    $encoder = $factory->getEncoder($user);
                    $password = $encoder->encodePassword(filter_var($form->getData(), FILTER_SANITIZE_STRING), $user->getSalt());
                    $user->setPassword($password);

                    $entityManager = $this->getDoctrine()->getManager();
                    $entityManager->persist($user);
                    $entityManager->flush();
                }
                return $this->render('AnsweredbySecurityBundle:Reset:post.html.twig');
            }
            return $this->render('AnsweredbySecurityBundle:Reset:index.html.twig', array(
                'username' => $user->getUsername(),
                'form' => $form->createView()
            ));
        }
    }
}
