<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="relationships")
 */
class Relationship extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="users_relationships")
     * @ORM\JoinColumn(name="origination_user_id", referencedColumnName="id", nullable=false)
     */
    protected $originationUser;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="User", inversedBy="relationships_with_user")
     * @ORM\JoinColumn(name="destination_user_id", referencedColumnName="id", nullable=false)
     */
    protected $destination_user;

    /**
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Network", inversedBy="relationships")
     * @ORM\JoinColumn(name="network_id", referencedColumnName="id", nullable=false)
     */
    protected $network;

    /**
     * @ORM\Column(name="md5", type="string", length=255, unique=true)
     */
    protected $md5;

    /**
     * @ORM\Column(name="approvedByOriginationUser", type="boolean")
     */
    protected $approvedByOriginationUser;

    /**
     * @ORM\Column(name="approvedByDestinationUser", type="boolean")
     */
    protected $approvedByDestinationUser;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\Column(name="ignoredByNonCreatedUser", type="boolean")
     */
    protected $ignoredByNonCreatedUser;

    /**
     * @ORM\Column(name="wasEmailSent", type="boolean")
     */
    protected $wasEmailSent;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser,
                                \Answeredby\EntityBundle\Entity\User $originationUser,
                                \Answeredby\EntityBundle\Entity\User $destinationUser,
                                \Answeredby\EntityBundle\Entity\Network $network)
    {
        $this->_setOriginationUser($originationUser);
        $this->_setDestinationUser($destinationUser);
        $this->_setNetwork($network);
        $this->_setMd5($this->getRandomToken());
        $this->setApprovedByOriginationUser(FALSE);
        $this->setApprovedByDestinationUser(FALSE);
        $this->setIgnoredByNonCreatedUser(FALSE);
        $this->setWasEmailSent(FALSE);
        parent::__construct($createdUser);
    }

    /**
     * Set originationUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Relationship
     */
    protected function _setOriginationUser(\Answeredby\EntityBundle\Entity\User $originationUser)
    {
        $this->originationUser = $originationUser;

        return $this;
    }

    /**
     * Get originationUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getOriginationUser()
    {
        return $this->originationUser;
    }

    /**
     * Set destination_user
     *
     * @param \Answeredby\EntityBundle\Entity\User $destinationUser
     * @return Relationship
     */
    protected function _setDestinationUser(\Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        $this->destination_user = $destinationUser;

        return $this;
    }

    /**
     * Get destination_user
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getDestinationUser()
    {
        return $this->destination_user;
    }
    
    /**
     * Set network
     *
     * @param \Answeredby\EntityBundle\Entity\Network $network
     * @return Relationship
     */
    protected function _setNetwork(\Answeredby\EntityBundle\Entity\Network $network)
    {
        $this->network = $network;

        return $this;
    }

    /**
     * Get network
     *
     * @return \Answeredby\EntityBundle\Entity\Network 
     */
    public function getNetwork()
    {
        return $this->network;
    }

    /**
     * Set md5
     *
     * @param string $md5
     * @return Relationship
     */
    protected function _setMd5($md5)
    {
        $this->md5 = $md5;

        return $this;
    }

    /**
     * Get md5
     *
     * @return string 
     */
    public function getMd5()
    {
        return $this->md5;
    }

    /**
     * Set approvedByOriginationUser
     *
     * @param boolean $approvedByOriginationUser
     * @return Relationship
     */
    public function setApprovedByOriginationUser($approvedByOriginationUser)
    {
        $this->approvedByOriginationUser = $approvedByOriginationUser;

        return $this;
    }

    /**
     * Get approvedByOriginationUser
     *
     * @return boolean
     */
    public function getApprovedByOriginationUser()
    {
        return $this->approvedByOriginationUser;
    }

    /**
     * Set approvedByDestinationUser
     *
     * @param boolean $approvedByDestinationUser
     * @return Relationship
     */
    public function setApprovedByDestinationUser($approvedByDestinationUser)
    {
        $this->approvedByDestinationUser = $approvedByDestinationUser;

        return $this;
    }

    /**
     * Get network
     *
     * @return boolean
     */
    public function getApprovedByDestinationUser()
    {
        return $this->approvedByDestinationUser;
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Relationship
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Set ignoredByNonCreatedUser
     *
     * @param boolean $ignoredByNonCreatedUser
     * @return Relationship
     */
    public function setIgnoredByNonCreatedUser($ignoredByNonCreatedUser)
    {
        $this->ignoredByNonCreatedUser = $ignoredByNonCreatedUser;

        return $this;
    }

    /**
     * Get ignoredByNonCreatedUser
     *
     * @return boolean
     */
    public function getIgnoredByNonCreatedUser()
    {
        return $this->ignoredByNonCreatedUser;
    }

    /**
     * Set wasEmailSent
     *
     * @param boolean $wasEmailSent
     * @return Relationship
     */
    public function setWasEmailSent($wasEmailSent)
    {
        $this->wasEmailSent = $wasEmailSent;

        return $this;
    }

    /**
     * Get wasEmailSent
     *
     * @return boolean
     */
    public function getWasEmailSent()
    {
        return $this->wasEmailSent;
    }

    /**
     * Get the User who isn't the createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User
     */
    public function getNonCreatedUser()
    {
        if( $this->getOriginationUser() == $this->getCreatedUser() )
            return $this->getDestinationUser();
        return $this->getOriginationUser();
    }
}
