<?php

namespace Answeredby\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Answeredby\EntityBundle\Entity\AbstractBaseEntity;
use Doctrine\Common\Collections\Criteria;

/**
 * @ORM\Entity
 * @ORM\Table(name="networks")
 */
class Network extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="text", type="text")
     */
    protected $text;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="networks")
     * @ORM\JoinColumn(name="created_user_id", referencedColumnName="id", nullable=false)
     */
    protected $createdUser;

    /**
     * @ORM\OneToMany(targetEntity="Relationship", mappedBy="network")
     **/
    protected $relationships;

    /**
     * @ORM\Column(name="isIndividualSharesType", type="boolean")
     */
    protected $isIndividualSharesType;

    /**
     * @ORM\Column(name="isDefaultType", type="boolean")
     */
    protected $isDefaultType;

    /**
     * @ORM\Column(name="isHidden", type="boolean")
     */
    protected $isHidden;

    /**
     * Constructor
     */
    public function __construct(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->setIsIndividualSharesType(FALSE);
        $this->setIsDefaultType(FALSE);
        $this->setIsHidden(FALSE);
        $this->relationships = new \Doctrine\Common\Collections\ArrayCollection();
        parent::__construct($createdUser);
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Network
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string 
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set createdUser
     *
     * @param \Answeredby\EntityBundle\Entity\User $createdUser
     * @return Network
     */
    protected function _setCreatedUser(\Answeredby\EntityBundle\Entity\User $createdUser)
    {
        $this->createdUser = $createdUser;

        return $this;
    }

    /**
     * Get createdUser
     *
     * @return \Answeredby\EntityBundle\Entity\User 
     */
    public function getCreatedUser()
    {
        return $this->createdUser;
    }

    /**
     * Add relationships
     *
     * @param \Answeredby\EntityBundle\Entity\Relationship $relationships
     * @return Network
     */
    public function addRelationship(\Answeredby\EntityBundle\Entity\Relationship $relationships)
    {
        $this->relationships[] = $relationships;

        return $this;
    }

    /**
     * Remove relationships
     *
     * @param \Answeredby\EntityBundle\Entity\Relationship $relationships
     */
    public function removeRelationship(\Answeredby\EntityBundle\Entity\Relationship $relationships)
    {
        $this->relationships->removeElement($relationships);
    }

    /**
     * Get relationships
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRelationships()
    {
        return $this->relationships;
    }

    /**
     * Get approvedRelationshipsWithOriginationUser
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApprovedRelationshipsWithOriginationUser(\Answeredby\EntityBundle\Entity\User $originationUser)
    {
        return $this->getRelationships()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("originationUser", $originationUser))
                ->andWhere(Criteria::expr()->eq("approvedByOriginationUser", TRUE))
                ->andWhere(Criteria::expr()->eq("approvedByDestinationUser", TRUE)));
    }

    /**
     * Get approvedRelationshipsWithDestinationUser
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getApprovedRelationshipsWithDestinationUser(\Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        return $this->getRelationships()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("destination_user", $destinationUser))
                ->andWhere(Criteria::expr()->eq("approvedByOriginationUser", TRUE))
                ->andWhere(Criteria::expr()->eq("approvedByDestinationUser", TRUE)));
    }

    /**
     * Get destinationUserApprovedRelationshipWithUsers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDestinationUserApprovedRelationshipWithUsers(\Answeredby\EntityBundle\Entity\User $originationUser,
                                                                    \Answeredby\EntityBundle\Entity\User $destinationUser)
    {
        return $this->getRelationships()->matching(Criteria::create()
                ->where(Criteria::expr()->eq("originationUser", $originationUser))
                ->andWhere(Criteria::expr()->eq("destination_user", $destinationUser))
                ->andWhere(Criteria::expr()->eq("approvedByDestinationUser", TRUE)));
    }

    /**
     * Set isIndividualSharesType
     *
     * @param boolean $isIndividualSharesType
     * @return Network
     */
    public function setIsIndividualSharesType($isIndividualSharesType)
    {
        $this->isIndividualSharesType = $isIndividualSharesType;

        return $this;
    }

    /**
     * Get isIndividualSharesType
     *
     * @return boolean 
     */
    public function getIsIndividualSharesType()
    {
        return $this->isIndividualSharesType;
    }

    /**
     * Set isIndividualSharesType
     *
     * @param boolean $isDefaultType
     * @return Network
     */
    public function setIsDefaultType($isDefaultType)
    {
        $this->isDefaultType = $isDefaultType;

        return $this;
    }

    /**
     * Get isDefaultType
     *
     * @return boolean 
     */
    public function getIsDefaultType()
    {
        return $this->isDefaultType;
    }

    /**
     * Set isHidden
     *
     * @param boolean $isHidden
     * @return Network
     */
    public function setIsHidden($isHidden)
    {
        $this->isHidden = $isHidden;

        return $this;
    }

    /**
     * Get isHidden
     *
     * @return boolean 
     */
    public function getIsHidden()
    {
        return $this->isHidden;
    }
}
