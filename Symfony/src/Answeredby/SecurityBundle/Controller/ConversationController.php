<?php

namespace Answeredby\SecurityBundle\Controller;

use Answeredby\SecurityBundle\Controller\SecurityBundleController;

class ConversationController extends SecurityBundleController
{   
    public function indexAction()
    {
        if( !$this->_isAuthenticated() )
            return $this->redirect($this->generateUrl('login') . "?target_url=" . $this->generateUrl('ConversationsPage', array(), TRUE));

        return $this->render('AnsweredbySecurityBundle:Conversation:index.html.twig');
    }
}
