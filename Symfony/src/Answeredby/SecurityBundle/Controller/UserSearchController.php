<?php

namespace Answeredby\SecurityBundle\Controller;

use Answeredby\SecurityBundle\Controller\SecurityBundleController;

class UserSearchController extends SecurityBundleController
{
    public function indexAction()
    {
        if( !$this->_isAuthenticated() )
            return $this->redirect($this->generateUrl('login') . "?target_url=" . $this->generateUrl('UserSearchPage', array(), TRUE));

        if( array_key_exists('username', $_GET) ){
            $users = $this->_getUserByUsernameSearch(filter_input(INPUT_GET, 'username', FILTER_SANITIZE_STRING), 1);
            if( $users && $users[0] )
                return $this->redirect($this->get('router')->generate('UserPage', array('userid' => $users[0]->getId(), 'username' => $users[0]->getUsername())), 303);
        }

        return $this->render('AnsweredbySecurityBundle:UserSearch:index.html.twig');
    }
}
