<?php

namespace Answeredby\ViewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PrivacyController extends Controller
{
    public function indexAction()
    {
        return $this->render('AnsweredbyViewBundle:Privacy:index.html.twig', array('doNotShowSidebar' => TRUE));
    }
}